//
//  ViewController.m
//  Pigulka
//
//  Created by macOS on 03.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "FormsViewController.h"
#import "SWRevealViewController.h"
#import "PCAngularActivityIndicatorView.h"
#import "InternetConnection.h"
#import "ErrorsView.h"
#import "MainViewController.h"
#import "User.h"

@interface FormsViewController () <UIWebViewDelegate, UIGestureRecognizerDelegate, SWRevealViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *formsWebView;

@property (weak, nonatomic) IBOutlet UIView *submitedView;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UILabel *navigationLabel;
@property (strong, nonatomic) PCAngularActivityIndicatorView *activity;
@property (strong, nonatomic) InternetConnection *internetConnection;
@property (weak, nonatomic) ErrorsView *errorView;

@end

@implementation FormsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [self.revealViewController tapGestureRecognizer];
    tap.delegate = self;
    
    [self.view addGestureRecognizer:tap];
    
    self.internetConnection = [[InternetConnection alloc] init];
    
    if ([self.internetConnection isOn] == NO) {
        self.errorView = [self createErrorView];
        [self.errorView internetConnectionError];
        [self.view addSubview:self.errorView];
        [self.activity removeFromSuperview];
        return;
    }

    if (self.navigationText) {
        self.navigationLabel.text = self.navigationText;
    }
    
    [self.submitedView setHidden:YES];
    
    NSArray *array = [UIApplication sharedApplication].delegate.window.rootViewController.childViewControllers;
    NSLog(@"%@",array);
    User *user = [User currentUser];
    
    //
//    self.requestString = [NSString stringWithFormat:@"https://anamnes.com.ua/api/v3/history_list?token=%@",user.token];
    //
    dispatch_queue_t queue = dispatch_queue_create("myqueue", NULL);
    dispatch_async(queue, ^{
        NSString *urlString = self.requestString;
        NSURL *url = [NSURL URLWithString:urlString];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.formsWebView loadRequest:urlRequest];
        });
        
    });
    
    [self.menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
//    UITapGestureRecognizer *dissmissTap = [self.revealViewController tapGestureRecognizer];
//    dissmissTap.delegate = self;
//    
//    [self.view addGestureRecognizer:dissmissTap];
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    self.activity = [[PCAngularActivityIndicatorView alloc] initWithActivityIndicatorStyle:PCAngularActivityIndicatorViewStyleLarge];
    self.activity.center = [self.view convertPoint:self.view.center fromView:self.view.superview];
    self.activity.color = [UIColor colorWithRed:0.0f/255.0f green:182.0f/255.0f blue:153.0f/255.0f alpha:1];
    [self.activity startAnimating];
    [self.view addSubview:self.activity];
    
    self.formsWebView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *webViewTapped = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
    webViewTapped.numberOfTapsRequired = 1;
    webViewTapped.delegate = self;
    [self.formsWebView addGestureRecognizer:webViewTapped];
    
    self.revealViewController.delegate = self;
    
}

//-(void)viewDidAppear:(BOOL)animated {
//    [super viewDidAppear:NO];
//    dispatch_queue_t queue = dispatch_queue_create("myqueue", NULL);
//    dispatch_async(queue, ^{
//        NSString *urlString = self.requestString;
//        NSURL *url = [NSURL URLWithString:urlString];
//        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
//        [self.formsWebView loadRequest:urlRequest];
//
//    });
//}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)tapAction:(UITapGestureRecognizer *)sender
{
    
    NSLog(@"touched");
}

-(void)viewWillAppear:(BOOL)animated {
    self.revealViewController.delegate = self;
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    
    UITapGestureRecognizer *webViewTapped = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
    webViewTapped.numberOfTapsRequired = 1;
    webViewTapped.delegate = self;
    webViewTapped = [self.revealViewController tapGestureRecognizer];
//    [self.formsWebView addGestureRecognizer:webViewTapped];
    
    if (position == FrontViewPositionRight) {
        [self.formsWebView addGestureRecognizer:webViewTapped];
    } else if (position == FrontViewPositionLeft) {
        [self.formsWebView removeGestureRecognizer:webViewTapped];
    }
}

- (ErrorsView *)createErrorView {
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"ErrorInternetView" owner:self options:nil];
    ErrorsView *errorView = [[ErrorsView alloc] init];
    errorView = [subviewArray objectAtIndex:0];
    errorView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 67, self.view.frame.size.width, self.view.frame.size.height);
    //    self.errorView = [self createErrorView];
    //    [self.errorView serverError];
    //    [self.view addSubview:self.errorView];
    return errorView;
}


//- (void)viewDidAppear:(BOOL)animated {
//    [super viewDidAppear:YES];
//    if ([self.internetConnection isOn] == NO) {
//        self.errorView = [self createErrorView];
//        [self.errorView internetConnectionError];
//        [self.view addSubview:self.errorView];
//        [self.activity removeFromSuperview];
//    }
//}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.activity removeFromSuperview];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {

}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSLog(@"url: %@", [[request URL] absoluteString]);
    //https://anamnes.com.ua
    if ([[[request URL] absoluteString] isEqualToString:@"https://anamnes.com.ua/api/v3/getpolicyfordoctor?"] || [[[request URL] absoluteString] isEqualToString:@"https://anamnes.com.ua/api/v3/success_worksheet?"]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MainViewController *log = [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
        log.toMyResearch = YES;
        [self.navigationController pushViewController:log animated:YES];
//        [self presentViewController:newView animated:YES completion:nil];
//        [self.navigationController popToViewController:log animated:YES];
        
//        [self.navigationController popViewControllerAnimated:YES];
    }
    
//    if (navigationType == UIWebViewNavigationTypeFormSubmitted) {
//        [self.formsWebView setHidden:YES];
//        [self.submitedView setHidden:NO];
//        
//        NSLog(@"Submited");
//        
//    }
    return YES;
}

@end
