//
//  RequestManager.m
//  Pigulka
//
//  Created by macOS on 06.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "RequestManager.h"
#import <AFNetworking.h>
#import "User.h"
#import "Research.h"
#import "Form.h"

//https://pigulka.uds.kiev.ua/api/v1/auth

static NSString *autorisation = @"https://anamnes.com.ua/api/v3/auth";
static NSString *genCode = @"https://anamnes.com.ua/api/v3/gencode";
static NSString *checkCode = @"https://anamnes.com.ua/api/v3/checkcode";
static NSString *resetPassword = @"https://anamnes.com.ua/api/v3/resetpassword";
static NSString *activeResearch = @"https://anamnes.com.ua/api/v3/getmytasklist";
static NSString *research = @"https://anamnes.com.ua/api/v3/gettasklist";
static NSString *myWorkSheets = @"https://anamnes.com.ua/api/v3/getmyworksheets";
static NSString *myProfile = @"https://anamnes.com.ua/api/v3/getmyprofile";
static NSString *changeTelephone = @"https://anamnes.com.ua/api/v3/changephone";
static NSString *saveProfile = @"https://anamnes.com.ua/api/v3/savemyprofile";
static NSString *newWork = @"https://anamnes.com.ua/api/v3/addworkplace";
static NSString *changeWork = @"https://anamnes.com.ua/api/v3/changeworkplace";
static NSString *getBonusWithPlace = @"https://anamnes.com.ua/api/v3/getmybonus";
static NSString *getUsers = @"https://anamnes.com.ua/api/v3/getmyauth";
static NSString *getAllResearch = @"https://anamnes.com.ua/api/v3/getalllists2019";
static NSString *setToken = @"https://anamnes.com.ua/api/v3/setFCMToken";

static NSString *historyList = @"https://anamnes.com.ua/api/v3/history_list";
static NSString *historyBonus = @"https://anamnes.com.ua/api/v3/history_bonus";

@implementation RequestManager

+ (RequestManager*)sharedManager {
    static RequestManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [RequestManager new];
    });
    return manager;
}

- (AFHTTPSessionManager*)sessionManager {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    return manager;
}

#pragma mark - Autorisation

- (void)autorisationUserFromTelephoneNumber:(NSString*)telephoneNumber
                                andPassword:(NSString*)password
                               successBlock:(void(^)(NSDictionary*success)) success
                              onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSDictionary *parameters = @{@"user" : @{@"phone" : telephoneNumber,
                                             @"password" : password}};
    
    AFHTTPSessionManager *manager = [self sessionManager];
    
    [manager POST:autorisation parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSHTTPURLResponse *statusCode= (NSHTTPURLResponse*)task.response;
        
        if (statusCode.statusCode == 200) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            if (success) {
                success(json);
            }
        } else {
            NSDictionary *dic = @{@"statusCode" : @"error"};
            success(dic);
//            fail(statusCode.statusCode);
            [self checkNeedCode:statusCode.statusCode];
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
        NSInteger statusCode = response.statusCode;
        if (statusCode) {
            fail(statusCode);
        }
        
    }];
    
}

#pragma mark - ForgotPassword

- (void)forgotPassword:(NSString*)telephoneNumber
          successBlock:(void(^)(NSDictionary*success)) success
         onFalureBlock:(void(^)(NSInteger statusCode)) fail {

    NSDictionary *parameters = @{@"user" : @{@"phone" : telephoneNumber}};
    
    AFHTTPSessionManager *manager = [self sessionManager];
    
    [manager POST:genCode parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSHTTPURLResponse *statusCode= (NSHTTPURLResponse*)task.response;
        
        if (statusCode.statusCode == 200) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            if (success) {
                success(json);
            }
        } else {
            [self checkNeedCode:statusCode.statusCode];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
        NSInteger statusCode = response.statusCode;
        if (statusCode) {
            fail(statusCode);
        }
    }];
}

#pragma mark - checkCode
- (void)checkCodeFromServer:(NSString*)code
                  withPhone:(NSString*)telephoneNumber
               successBlock:(void(^)(NSDictionary*success)) success
              onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSDictionary *parameters = @{@"user" : @{@"phone" : telephoneNumber,
                                             @"code" : code}};
    
    AFHTTPSessionManager *manager = [self sessionManager];
    
    [manager POST:checkCode parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSHTTPURLResponse *statusCode= (NSHTTPURLResponse*)task.response;
        
        if (statusCode.statusCode == 200) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            if (success) {
                success(json);
            }
        } else {
            [self checkNeedCode:statusCode.statusCode];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
        NSInteger statusCode = response.statusCode;
        if (statusCode) {
            fail(statusCode);
        }
    }];
    
}

- (void)resetOldPasswordFromNew:(NSString*)newPassword
                       withCode:(NSString*)code
                 andPhoneNumber:(NSString*)telephoneNumber
               successBlock:(void(^)(NSDictionary*success)) success
              onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSDictionary *parameters = @{@"user" : @{@"phone" : telephoneNumber,
                                             @"code" : code,
                                             @"new_password" : newPassword}};
    
    AFHTTPSessionManager *manager = [self sessionManager];
    
    [manager POST:resetPassword parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSHTTPURLResponse *statusCode= (NSHTTPURLResponse*)task.response;
        
        if (statusCode.statusCode == 200) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            if (success) {
                success(json);
            }
        } else {
            [self checkNeedCode:statusCode.statusCode];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
        NSInteger statusCode = response.statusCode;
        if (statusCode) {
            fail(statusCode);
        }
    }];

}

- (void)getAccounts:(void(^)(NSDictionary *success)) success
      onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    AFHTTPSessionManager *manager = [self sessionManager];
    
    User *user = [User currentUser];
    
    NSString *request = [NSString stringWithFormat:@"%@?token=%@",getUsers,user.token];
    
//    NSDictionary *parameters = @{@"user" : @{@"token" : user.token}};
    
    [manager GET:request parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSHTTPURLResponse *statusCode= (NSHTTPURLResponse*)task.response;
        
        if (statusCode.statusCode == 200) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            NSDictionary *mainDict = [json valueForKey:@"data"];
            if (success) {
                success(mainDict);
            }
            
        } else {
            [self checkNeedCode:statusCode.statusCode];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
        NSInteger statusCode = response.statusCode;
        if (statusCode) {
            fail(statusCode);
        }
    }];
}

#pragma mark - getBonusWithPlace

- (void)getBonusWithPlaceSuccesBlock:(void(^)(NSDictionary *success)) success
                       onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    AFHTTPSessionManager *manager = [self sessionManager];
    
    User *user = [User currentUser];
    
    if (user == nil) { return; }
    
    NSDictionary *parameters = @{@"user" : @{@"token" : user.token}};
    
    [manager POST:getBonusWithPlace parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSHTTPURLResponse *statusCode= (NSHTTPURLResponse*)task.response;
        
        if (statusCode.statusCode == 200) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            NSDictionary *mainDict = [json valueForKey:@"user"];
            if (success) {
                success(mainDict);
            }
            
        } else {
            [self checkNeedCode:statusCode.statusCode];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
        NSInteger statusCode = response.statusCode;
        if (statusCode) {
            fail(statusCode);
        }
    }];
}

- (void)sendToken:(NSString*)fcmtoken
      SuccesBlock:(void(^)(NSDictionary *success)) success
    onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    AFHTTPSessionManager *manager = [self sessionManager];
    
    User *user = [User currentUser];
    
    if (user == nil) { return; }
    
    NSDictionary *parameters = @{@"user" : @{@"token" : user.token,
                                             @"fcmtoken" : fcmtoken
                                             }};
    
    [manager POST:setToken parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSHTTPURLResponse *statusCode= (NSHTTPURLResponse*)task.response;
        
        if (statusCode.statusCode == 200) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            NSDictionary *mainDict = [json valueForKey:@"user"];
            if (success) {
                success(mainDict);
            }
            
        } else {
            [self checkNeedCode:statusCode.statusCode];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

    }];
}

#pragma mark - Research
- (void)getAllResearchsuccessBlock:(void(^)(NSArray *success)) success
                           withTestId:(void(^)(NSInteger testId)) testId
                         andGeoEneble:(void(^)(BOOL geoEnable)) geoEnable
                        onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    AFHTTPSessionManager *manager = [self sessionManager];
    
    User *user = [User currentUser];
    
    if (user == nil) { return; }
    
    NSDictionary *parameters = @{@"user" : @{@"token" : user.token}};
    
    [manager POST:getAllResearch parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSHTTPURLResponse *statusCode= (NSHTTPURLResponse*)task.response;
        
        if (statusCode.statusCode == 200) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            NSDictionary *mainDict = [[json valueForKey:@"user"] valueForKey:@"tasks"];
            NSMutableArray *array = [NSMutableArray new];
            NSInteger userId = [[[json valueForKey:@"user"] valueForKey:@"user_id"] integerValue];
            BOOL respGeoEnable = [[[json valueForKey:@"user"] valueForKey:@"geo_enable"] boolValue];
            for (NSDictionary *dic in mainDict) {
                Research *research = [[Research alloc] initWithServerResponse:dic];
//                research.isOtherResearch = NO;
                [array addObject:research];
            }
            if (success) {
                success(array);
            }
            if (testId) {
                testId(userId);
            }
            if (geoEnable) {
                geoEnable(respGeoEnable);
            }
            
        } else {
            [self checkNeedCode:statusCode.statusCode];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
        NSInteger statusCode = response.statusCode;
        if (statusCode) {
            fail(statusCode);
        }
    }];
    
}

- (void)getActiveResearchsuccessBlock:(void(^)(NSArray *success)) success
                           withTestId:(void(^)(NSInteger testId)) testId
                         andGeoEneble:(void(^)(BOOL geoEnable)) geoEnable
                        onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    AFHTTPSessionManager *manager = [self sessionManager];
    
    User *user = [User currentUser];
    
    if (user == nil) { return; }
    
    NSDictionary *parameters = @{@"user" : @{@"token" : user.token}};
    
    [manager POST:activeResearch parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSHTTPURLResponse *statusCode= (NSHTTPURLResponse*)task.response;
        
        if (statusCode.statusCode == 200) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            NSDictionary *mainDict = [[json valueForKey:@"user"] valueForKey:@"tasks"];
            NSMutableArray *array = [NSMutableArray new];
            NSInteger userId = [[[json valueForKey:@"user"] valueForKey:@"user_id"] integerValue];
            BOOL respGeoEnable = [[[json valueForKey:@"user"] valueForKey:@"geo_enable"] boolValue];
            for (NSDictionary *dic in mainDict) {
                Research *research = [[Research alloc] initWithServerResponse:dic];
                research.isOtherResearch = NO;
                [array addObject:research];
            }
            if (success) {
                success(array);
            }
            if (testId) {
                testId(userId);
            }
            if (geoEnable) {
                geoEnable(respGeoEnable);
            }
            
        } else {
            [self checkNeedCode:statusCode.statusCode];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
        NSInteger statusCode = response.statusCode;
        if (statusCode) {
            fail(statusCode);
        }
    }];
}


- (void)getResearchsuccessBlock:(void(^)(NSArray *success)) success
                        onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    AFHTTPSessionManager *manager = [self sessionManager];
    
    User *user = [User currentUser];
    
    if (user == nil) { return; }
    
    NSDictionary *parameters = @{@"user" : @{@"token" : user.token}};
    
    [manager POST:research parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSHTTPURLResponse *statusCode= (NSHTTPURLResponse*)task.response;
        
        if (statusCode.statusCode == 200) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            NSDictionary *mainDict = [[json valueForKey:@"user"] valueForKey:@"tasks"];
            NSMutableArray *array = [NSMutableArray new];
            for (NSDictionary *dic in mainDict) {
                Research *research = [[Research alloc] initWithServerResponse:dic];
                research.isOtherResearch = YES;
                [array addObject:research];
            }
            if (success) {
                success(array);
            }
            
        } else {
            [self checkNeedCode:statusCode.statusCode];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
        NSInteger statusCode = response.statusCode;
        if (statusCode) {
            fail(statusCode);
        }
    }];
}

#pragma mark - newWork

- (void)addNewWork:(NSString *)work
       withAddress:(NSString*)address
        longtitude:(double)longtitude
          latitude:(double)latitude
       succesBlock:(void(^)(NSInteger success)) success
     onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    AFHTTPSessionManager *manager = [self sessionManager];
    
    User *user = [User currentUser];
    
    if (user == nil) { return; }
    
    NSMutableArray *array = [NSMutableArray new];
    
    NSDictionary *dic = @{@"name" : work,
                          @"address" : address,
                          @"latitude" : @(latitude),
                          @"longitude" : @(longtitude)};
    [array addObject:dic];
    
    NSDictionary *parameters = @{@"user" : @{@"token" : user.token,
                                             @"user_data" : @{
                                                     @"places" : array}}};
    
    [manager POST:newWork parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSHTTPURLResponse *statusCode= (NSHTTPURLResponse*)task.response;
        
        if (statusCode.statusCode == 200) {
            if (success) {
                success(statusCode.statusCode);
            }
            
        } else {
            [self checkNeedCode:statusCode.statusCode];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
        NSInteger statusCode = response.statusCode;
        if (statusCode) {
            fail(statusCode);
        }
    }];
}


- (void)changeWork:(NSString *)work
       withAddress:(NSString*)address
           placeId:(NSNumber*)placeId
        longtitude:(double)longtitude
          latitude:(double)latitude
            status:(int)status
       succesBlock:(void(^)(NSInteger success)) success
     onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    AFHTTPSessionManager *manager = [self sessionManager];
    
    User *user = [User currentUser];
    
    if (user == nil) { return; }
    
    NSMutableArray *array = [NSMutableArray new];
    
    NSDictionary *dic = @{@"name" : work,
                          @"address" : address,
                          @"latitude" : @(latitude),
                          @"longitude" : @(longtitude),
                          @"status" : @(status),
                          @"id" : placeId
                          };
    [array addObject:dic];
    
    NSDictionary *parameters = @{@"user" : @{@"token" : user.token,
                                             @"user_data" : @{
                                                     @"places" : array}}};
    
    [manager POST:changeWork parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSHTTPURLResponse *statusCode= (NSHTTPURLResponse*)task.response;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode.statusCode == 200) {
            if (success) {
                success(statusCode.statusCode);
            }
            
        } else {
            [self checkNeedCode:statusCode.statusCode];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
        NSInteger statusCode = response.statusCode;
        if (statusCode) {
            fail(statusCode);
        }
    }];
}

- (void)getHistoryList:(void(^)(NSString *text)) text
         onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    AFHTTPSessionManager *manager = [self sessionManager];
    User *user = [User currentUser];
    
    if (user == nil) { return; }
    
    NSDictionary *parameters = @{@"user" : @{@"token" : user.token}};
    
    [manager POST:historyList parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSHTTPURLResponse *statusCode= (NSHTTPURLResponse*)task.response;
        
        if (statusCode.statusCode == 200) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            NSString *htmlString = [[json valueForKey:@"user"] valueForKey:@"html"];
            text(htmlString);
            
        } else {
            [self checkNeedCode:statusCode.statusCode];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
        NSInteger statusCode = response.statusCode;
        if (statusCode) {
            fail(statusCode);
        }
    }];
}

- (void)getHistoryBonus:(void(^)(NSString *text)) text
          onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    AFHTTPSessionManager *manager = [self sessionManager];
    User *user = [User currentUser];
    
    if (user == nil) { return; }
    
    NSDictionary *parameters = @{@"user" : @{@"token" : user.token}};
    
    [manager POST:historyBonus parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSHTTPURLResponse *statusCode= (NSHTTPURLResponse*)task.response;
        
        if (statusCode.statusCode == 200) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            NSString *htmlString = [[json valueForKey:@"user"] valueForKey:@"html"];
            text(htmlString);
            
        } else {
            [self checkNeedCode:statusCode.statusCode];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
        NSInteger statusCode = response.statusCode;
        if (statusCode) {
            fail(statusCode);
        }
    }];
}

#pragma mark - UserWorkSheets
- (void)getUserWorkSheetsWithBonusessText:(void(^)(NSString *text)) text
                                companyId:(void(^)(NSNumber *companyId)) companyId
                                bonuesURL:(void(^)(NSString *bonusesURL)) bonusesURL
                          howUseBonuesURL:(void(^)(NSString *bonusesURL)) howUseBonues
                              sucessBlock:(void(^)(NSArray *success)) success
                            onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    AFHTTPSessionManager *manager = [self sessionManager];
    
    User *user = [User currentUser];
    
    if (user == nil) { return; }
    
    NSDictionary *parameters = @{@"user" : @{@"token" : user.token}};
    
    [manager POST:myWorkSheets parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSHTTPURLResponse *statusCode= (NSHTTPURLResponse*)task.response;
        
        if (statusCode.statusCode == 200) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            NSNumber *number = [[json valueForKey:@"user"] valueForKey:@"compnay_id"];
            NSString *bonusessText = [[[json valueForKey:@"user"] valueForKey:@"user_data"] valueForKey:@"text"];
            NSString *bonusessURL = [[[json valueForKey:@"user"] valueForKey:@"user_data"] valueForKey:@"bonus_url"];
            NSString *howUsebonusessURL = [[[json valueForKey:@"user"] valueForKey:@"user_data"] valueForKey:@"how_use_bonus"];
            NSDictionary *mainDict = [[[json valueForKey:@"user"] valueForKey:@"user_data"] valueForKey:@"worksheets"];
            NSMutableArray *array = [NSMutableArray new];
            for (NSDictionary *dic in mainDict) {
                Form *form = [[Form alloc] initWithServerResponse:dic];
                [array addObject:form];
            }
            if (success) {
                companyId(number);
                success(array);
                text(bonusessText);
                bonusesURL(bonusessURL);
                howUseBonues(howUsebonusessURL);
            }
            
        } else {
            [self checkNeedCode:statusCode.statusCode];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
        NSInteger statusCode = response.statusCode;
        if (statusCode) {
            fail(statusCode);
        }
    }];
}


#pragma mark - changeTelephoneNumber
- (void)chengeTelephoneNumber:(NSString *)telephoneNumber
                  succesBlock:(void(^)(NSInteger success)) success
                onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    AFHTTPSessionManager *manager = [self sessionManager];
    
    User *user = [User currentUser];
    
    if (user == nil) { return; }
    
    NSDictionary *parameters = @{@"user" : @{@"token" : user.token,
                                             @"user_data" : @{
                                                     @"phone" : telephoneNumber}}};
    
    [manager POST:changeTelephone parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSHTTPURLResponse *statusCode= (NSHTTPURLResponse*)task.response;
        
        if (statusCode.statusCode == 200) {
            if (success) {
                success(statusCode.statusCode);
            }
            
        } else {
            [self checkNeedCode:statusCode.statusCode];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
        NSInteger statusCode = response.statusCode;
        if (statusCode) {
            fail(statusCode);
        }
    }];
}

#pragma mark - getMyProfile
- (void)getMyProfileSuccessBlock:(void(^)(UserFromPlases *success)) success
                   onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    AFHTTPSessionManager *manager = [self sessionManager];
    
    User *user = [User currentUser];
    
    if (user == nil) { return; }
    
    NSDictionary *parameters = @{@"user" : @{@"token" : user.token}};
    
    [manager POST:myProfile parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSHTTPURLResponse *statusCode= (NSHTTPURLResponse*)task.response;
        
        if (statusCode.statusCode == 200) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            NSDictionary *mainDict = [[json valueForKey:@"user"] valueForKey:@"user_data"];
            UserFromPlases *user = [[UserFromPlases alloc] initWithServerResponse:mainDict];
            if (success) {
                success(user);
            }
            
        } else {
            [self checkNeedCode:statusCode.statusCode];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
        NSInteger statusCode = response.statusCode;
        if (statusCode) {
            fail(statusCode);
        }
    }];
}

#pragma mark - saveProfile
- (void)saveProfileWithProfession:(NSString*)profession
                       experiance:(NSString*)experience
                      andPosition:(NSString*)position
                      succesBlock:(void(^)(NSInteger success)) success
                  onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    AFHTTPSessionManager *manager = [self sessionManager];
    
    User *user = [User currentUser];
    
    if (user == nil) { return; }
    
    NSDictionary *parameters = @{@"user" : @{@"token" : user.token,
                                             @"user_data" : @{
                                                     @"profession" : profession,
                                                     @"experience" : experience,
                                                     @"position" : position,}}};
    
    [manager POST:saveProfile parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSHTTPURLResponse *statusCode= (NSHTTPURLResponse*)task.response;
        
        if (statusCode.statusCode == 200) {
            if (success) {
                success(statusCode.statusCode);
            }
            
        } else {
            [self checkNeedCode:statusCode.statusCode];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
        NSInteger statusCode = response.statusCode;
        if (statusCode) {
            fail(statusCode);
        }
    }];
}

#pragma mark - code

- (void)checkNeedCode:(NSInteger)statusCode {
    if (statusCode == 201) {
        [self showAllertViewWithTittle:@"Помилка" andMessage:@"Неправильний пароль!"];
        return;
    }
    if (statusCode == 202) {
        [self showAllertViewWithTittle:@"Помилка" andMessage:@"Такого юзера не існує"];
        return;
    }
    if (statusCode == 203) {
        [self showAllertViewWithTittle:@"Помилка" andMessage:@"Неправильний код підтвердження"];
        return;
    }
}


- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    UIAlertView *templateAlertView = [[UIAlertView alloc] initWithTitle:tittle
                                                                message:message
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil, nil];
    templateAlertView.alertViewStyle = UIAlertViewStyleDefault;
    [templateAlertView show];
}

@end
