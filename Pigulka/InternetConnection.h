//
//  InternetConnection.h
//  Pigulka
//
//  Created by macOS on 27.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InternetConnection : NSObject

- (BOOL)isOn;

@end
