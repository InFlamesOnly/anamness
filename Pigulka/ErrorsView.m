//
//  ErrorsView.m
//  Pigulka
//
//  Created by macOS on 27.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "ErrorsView.h"

@implementation ErrorsView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}

- (void)internetConnectionError {
    [self.errorImage setImage:[UIImage imageNamed:@"wifi"]];
    self.errorTittle.text = @"Ви не підключені до Інтернету!";
    self.errorMassage.text = @"Необхідно підключитися до Інтернету або перевірити підключення до мережі WI-FI";
    [self.toMyResearchButton setHidden:YES];
}

- (void)changeWork {
    [self.errorImage setImage:[UIImage imageNamed:@"ok"]];
    self.errorTittle.text = @"Вашу заявку на зміну місця роботи надіслано!";
    self.errorMassage.text = @" ";
    [self.toMyResearchButton setHidden:YES];
}

- (void)serverError {
    [self.errorImage setImage:[UIImage imageNamed:@"internet"]];
    self.errorTittle.text = @"З'єднання перервано, переконайтеся, що підключено до Інтернету!";
    [self.errorMassage setHidden:YES];
    [self.toMyResearchButton setHidden:YES];
}

- (void)noResearchActive {
    [self.errorImage setImage:[UIImage imageNamed:@"noResearch"]];
    self.errorTittle.text = @"Це дослідження завершено. Перейдіть до іншого дослідження.";
    [self.errorMassage setHidden:YES];
    [self.toMyResearchButton setHidden:YES];
    
}
- (void)noResearch {
    [self.errorImage setImage:[UIImage imageNamed:@"noResearch"]];
    self.errorTittle.text = @"На даний момент часу інших доступних для Вас досліджень немає, перейдіть до розділу “Мої дослідження!“";
    [self.errorMassage setHidden:YES];
    [self.toMyResearchButton setHidden:NO];
}

-(IBAction)goToMyResearch:(id)sender {
    NSLog(@"GOTORESEARCH");
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:YES forKey:@"isTapToMyReciearch"];
    [userDefaults synchronize];
    [self.delegate goToMeResearch];
}

@end
