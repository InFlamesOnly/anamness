//
//  BonuseCell.h
//  Pigulka
//
//  Created by macOS on 26.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BonuseCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *bonusDescription;
@property (weak, nonatomic) IBOutlet UILabel *bonusActive;
@property (weak, nonatomic) IBOutlet UILabel *text;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UIView *activeView;
@property (weak, nonatomic) IBOutlet UIView *backgroundShadowView;

@end
