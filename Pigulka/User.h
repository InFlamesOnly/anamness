//
//  User.h
//  Pigulka
//
//  Created by macOS on 14.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>
#import "RealmPlace.h"

@interface User : RLMObject

@property NSString *phone;
@property NSString *middleName;
@property NSString *name;
@property NSString *token;
@property NSString *bonuses;

@property (nonatomic, strong, readwrite) RLMArray<RealmPlace *>< RealmPlace> *place;

+ (void)saveUser:(User *)user;
+ (User *)currentUser;
+ (void)updateUser:(User*)newUser;


- (id)initWithServerResponse:(NSDictionary*)response;
//- (id)initWithCoder:(NSCoder *)decoder;
//- (void)encodeWithCoder:(NSCoder *)encoder ;
//
//- (void)saveCustomObject:(User *)object key:(NSString *)key;
//- (User *)loadCustomObjectWithKey:(NSString *)key;
//- (void)resetDefaults;

@end
