//
//  BonusesViewController.m
//  Pigulka
//
//  Created by macOS on 25.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "BonusesViewController.h"
#import "BonuseCell.h"
#import "RequestManager.h"
#import "Form.h"
#import "BonusesWebViewController.h"
#import "SWRevealViewController.h"
#import "PCAngularActivityIndicatorView.h"
#import "ErrorsView.h"
#import "InternetConnection.h"
#import "UITableViewCell+SettingCell.h"
#import "HistoryBonusessCell.h"
#import "HistoryFormsCell.h"

@interface BonusesViewController () <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, SWRevealViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *tittleText;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UITableView *bonusesTableView;
@property (strong, nonatomic) NSString *bonusesURL;
@property (strong, nonatomic) NSString *howUseBonusesURL;

@property (strong, nonatomic) NSArray *formsArray;
@property (strong, nonatomic) PCAngularActivityIndicatorView *activity;


@property (weak, nonatomic) IBOutlet UIButton *howUseBonusses;
@property (weak, nonatomic) IBOutlet UIButton *useBonusses;

@property (strong, nonatomic) InternetConnection *internetConnection;

@property (weak, nonatomic) ErrorsView *errorView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *showBonuses;

@property NSString *historyListString;
@property NSString *historyBonusString;


@end


@implementation BonusesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.internetConnection = [[InternetConnection alloc] init];
    
    [self.bonusesTableView setHidden:YES];
    self.bonusesURL = [NSString new];
    self.howUseBonusesURL = [NSString new];
    
    [[RequestManager sharedManager] getUserWorkSheetsWithBonusessText:^(NSString *text) {
        self.tittleText.text = text;
    } companyId:^(NSNumber *companyId) {
       
    } bonuesURL:^(NSString *bonusesURL) {
         self.bonusesURL = bonusesURL;
    } howUseBonuesURL:^(NSString *bonusesURL) {
        self.howUseBonusesURL = bonusesURL;
    } sucessBlock:^(NSArray *success) {
        self.formsArray = success;
        
        [self getHistoryListText];

    } onFalureBlock:^(NSInteger statusCode) {
        [self checkServerError];
    }];
    
    [self.menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    self.activity = [[PCAngularActivityIndicatorView alloc] initWithActivityIndicatorStyle:PCAngularActivityIndicatorViewStyleLarge];
    self.activity.center = [self.view convertPoint:self.view.center fromView:self.view.superview];
    self.activity.color = [UIColor colorWithRed:0.0f/255.0f green:182.0f/255.0f blue:153.0f/255.0f alpha:1];
    [self.activity startAnimating];
    [self.view addSubview:self.activity];
    
    if ([self.internetConnection isOn] == NO) {
        self.errorView = [self createErrorView];
        [self.errorView internetConnectionError];
        [self.view addSubview:self.errorView];
        [self.activity removeFromSuperview];
    }
    
}

- (void)getHistoryListText {
    [[RequestManager sharedManager] getHistoryList:^(NSString *text) {
        self.historyListString = text;
        [self getHistoryBonusText];
    } onFalureBlock:^(NSInteger statusCode) {
        
    }];
}

- (void)getHistoryBonusText {
    [[RequestManager sharedManager] getHistoryBonus:^(NSString *text) {
        self.historyBonusString = text;
        [self.bonusesTableView reloadData];
        [self.bonusesTableView setHidden:NO];
        [self.activity removeFromSuperview];
    } onFalureBlock:^(NSInteger statusCode) {
        
    }];
}

-(void)viewWillAppear:(BOOL)animated {
    self.revealViewController.delegate = self;
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
    tap = [self.revealViewController tapGestureRecognizer];
    tap.delegate = self;
    if (position == FrontViewPositionRight) {
        
        [self.view addGestureRecognizer:tap];
    } else if (position == FrontViewPositionLeft) {
        [self.view removeGestureRecognizer:tap];
    }
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:NO];
    [[self bonusesTableView] reloadData];

    //    [self.researchTableView reloadData];
    //    [self viewDidLayoutSubviews];
    //    [self.researchTableView layoutSubviews];
}
- (void)checkServerError {
    if ([self.internetConnection isOn]) {
        self.errorView = [self createErrorView];
        [self.errorView serverError];
        [self.view addSubview:self.errorView];
    } else {
        self.errorView = [self createErrorView];
        [self.errorView internetConnectionError];
        [self.view addSubview:self.errorView];
    }
}

- (ErrorsView *)createErrorView {
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"ErrorInternetView" owner:self options:nil];
    ErrorsView *errorView = [[ErrorsView alloc] init];
    errorView = [subviewArray objectAtIndex:0];
    errorView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 67, self.view.frame.size.width, self.view.frame.size.height);
    //    self.errorView = [self createErrorView];
    //    [self.errorView serverError];
    //    [self.view addSubview:self.errorView];
    return errorView;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    BonusesWebViewController *vc = [segue destinationViewController];
    if ([segue.identifier isEqualToString:@"howUseBonus"]) {
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        vc.isUseBonus = YES;
        vc.bonusURL = self.howUseBonusesURL;
    }
    if ([segue.identifier isEqualToString:@"useBonus"]) {
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        vc.bonusURL = self.bonusesURL;
    }
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.formsArray.count + 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        HistoryBonusessCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryBonusessCell"];
        NSString * htmlString = self.historyBonusString;
        NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        cell.htmlText.attributedText = attrStr;
        [UITableViewCell addShadowToView:cell.backgroundShadowView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    if (indexPath.row == 1) {
        HistoryFormsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HistoryFormsCell"];
        NSString * htmlString = self.historyListString;
        NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        cell.htmlText.attributedText = attrStr;
        [UITableViewCell addShadowToView:cell.backgroundShadowView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    //TiitleCell
    
    if (indexPath.row == 2) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TiitleCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    BonuseCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BonuseCell"];
    Form *form = self.formsArray[indexPath.row - 3];
    cell.bonusDescription.text = form.text;
    cell.text.text = form.name;
    cell.date.text = form.date;
    
    if (form.status == 1) {
        cell.bonusActive.text = @"Заявку підтверджено!";
        [cell.activeView setBackgroundColor:[UIColor greenColor]];
    }
    if (form.status == 0) {
        cell.bonusActive.text = @"Заявка не підтверджена!";
        [cell.activeView setBackgroundColor:[UIColor yellowColor]];
    }
    if (form.status == 2) {
        cell.bonusActive.text = @"Заявку відхилено!";
        [cell.activeView setBackgroundColor:[UIColor redColor]];
        cell.bonusDescription.textColor = [UIColor redColor];
    }
    
    [UITableViewCell addShadowToView:cell.backgroundShadowView];
    [UITableViewCell roundeWithWhiteLayer:cell.activeView];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

//#import "HistoryBonusessCell.h"
//#import "HistoryFormsCell.h"

@end
