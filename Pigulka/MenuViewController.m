//
//  MenuViewController.m
//  Pigulka
//
//  Created by macOS on 12.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "MenuViewController.h"
#import "SWRevealViewController.h"
#import "MenuCell.h"
#import "User.h"
#import "AutorizationViewController.h"
#import "MainViewController.h"
#import "User.h"

@interface MenuViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) User *user;

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.revealViewController.frontViewShadowOffset = CGSizeMake(0, 0);
    self.revealViewController.frontViewShadowOpacity = 0.0f;
    self.revealViewController.frontViewShadowRadius = 0.0f;
    
    if ([User allObjects].count != 0) {
        self.user = [User currentUser];
        self.name.text = [NSString stringWithFormat:@"%@ %@", self.user.name , self.user.middleName];
    }
    [self addGradientToView:self.headerView];
}

- (void)viewDidAppear:(BOOL)animated {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//    [userDefaults setBool:YES forKey:@"isTapToMyReciearch"];
//    [userDefaults synchronize];
//    if ([[userDefaults valueForKey:@"isTapToMyReciearch"] boolValue] == YES) {
//
//        NSIndexPath *indexPaths = self.menuTableView.indexPathForSelectedRow;
//        if (indexPaths) {
//            [self.menuTableView deselectRowAtIndexPath:indexPaths animated:animated];
//        }
//
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//        [self tableView:self.menuTableView didSelectRowAtIndexPath:indexPath];
//        MenuCell *cell = [self.menuTableView cellForRowAtIndexPath:indexPath];
//        UIView *bgColorView = [[UIView alloc] init];
//        bgColorView.backgroundColor = [UIColor colorWithRed:232.0f/255.0f green:255.0f/255.0f blue:250.0f/255.0f alpha:1];
//        [cell setSelectedBackgroundView:bgColorView];
//    }

}

- (void)addGradientToView:(UIView *)view {
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = view.bounds;
    
    UIColor *lightGreen = [UIColor colorWithRed:0.0f/255.0f green:241.0f/255.0f blue:191.0f/255.0f alpha:1];
    UIColor *green = [UIColor colorWithRed:0.0f/255.0f green:182.0f/255.0f blue:153.0f/255.0f alpha:1];
    
    gradient.colors = @[(id)lightGreen.CGColor, (id)green.CGColor];
    
    [view.layer insertSublayer:gradient atIndex:0];
}

- (void)selectMyresearch {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:4 inSection:0];
//    [self.menuTableView selectRowAtIndexPath:indexPath
//                                      animated:YES
//                                scrollPosition:UITableViewScrollPositionNone];
//    [self.menuTableView reloadData];
    [self tableView:self.menuTableView didSelectRowAtIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
//    if (indexPath.row != 0 && [[userDefaults valueForKey:@"isTapToMyReciearch"] boolValue] == YES) {
//
//    } else {
//
//    }
    
    MenuCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    cell.cellName.textColor = [UIColor colorWithRed:0.0f/255.0f green:191.0f/255.0f blue:158.0f/255.0f alpha:1];
    if ([cell.cellName.text isEqualToString:@"Вийти з Anamnes"]) {
        [[RLMRealm defaultRealm] beginWriteTransaction];
        [[RLMRealm defaultRealm] deleteAllObjects];
        [[RLMRealm defaultRealm] commitWriteTransaction];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setBool:NO forKey:@"isFirst"];
        [userDefaults synchronize];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *vc = [storyboard instantiateViewControllerWithIdentifier:@"AutorizationViewController"];
        if (@available(iOS 13.0, *)) {
            [vc setModalPresentationStyle: UIModalPresentationFullScreen];
        }
        [self presentViewController:vc animated:YES completion:nil];
        NSArray *array = [UIApplication sharedApplication].delegate.window.rootViewController.childViewControllers;
        NSLog(@"%@",array);
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"isOtherResearch"]) {
        UINavigationController *navController = [segue destinationViewController];
        MainViewController *vc = (MainViewController *)([navController viewControllers][0]);
        vc.isOtherResearch = YES;
    }
    if ([segue.identifier isEqualToString:@"isMyResearch"]) {
        UINavigationController *navController = [segue destinationViewController];
        MainViewController *vc = (MainViewController *)([navController viewControllers][0]);
        vc.isMyResearch = YES;
    }
    if ([segue.identifier isEqualToString:@"quit"]) {

        
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        return 0;
    }
    if (indexPath.row == 5) {
        return 0;
    } else {
        return 54;
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    MenuCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.cellName.textColor = [UIColor blackColor];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MenuCell *cell = [[MenuCell alloc] init];
    if (indexPath.row == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"MenuCell"];
    } else if (indexPath.row == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"MenuCell1"];
    } else if (indexPath.row == 2) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"MenuCell2"];
    } else if (indexPath.row == 3) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"MenuCell3"];
        cell.cellName.text = [NSString stringWithFormat:@"Мій рейтинг (%@)",self.user.bonuses];
    } else if (indexPath.row == 4) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"MenuCell4"];
    } else if (indexPath.row == 5) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"MenuCell5"];
    } else if (indexPath.row == 6) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"MenuCell6"];
    } else if (indexPath.row == 7) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"MenuCell7"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;\

//    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
//    UIView *bgColorView = [[UIView alloc] init];
//    bgColorView.backgroundColor = [UIColor colorWithRed:232.0f/255.0f green:255.0f/255.0f blue:250.0f/255.0f alpha:1];
//    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

@end
