//
//  RoundedViewWithGreenBorder.h
//  Pigulka
//
//  Created by macOS on 05.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoundedViewWithGreenBorder : UIView

- (void) borderOn;
- (void) borderOff;

@end
