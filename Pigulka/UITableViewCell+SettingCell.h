//
//  UITableViewCell+SettingCell.h
//  Pigulka
//
//  Created by macOS on 18.10.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (SettingCell)

+ (void)addGradientToView:(UIView *)view;
+ (void)addShadowToView:(UIView*)view;
+ (void)roundeWithWhiteLayer:(UIView*)view;


@end
