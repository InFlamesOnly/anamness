//
//  MainViewController.m
//  Pigulka
//
//  Created by macOS on 10.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "MainViewController.h"
#import "ResearchCell.h"
#import "SWRevealViewController.h"
#import "RequestManager.h"
#import "Research.h"
#import "FormsViewController.h"
#import "PCAngularActivityIndicatorView.h"
#import <CoreLocation/CoreLocation.h>
#import "User.h"
#import "ErrorsView.h"
#import "InternetConnection.h"
#import "UITableViewCell+SettingCell.h"
#import "MenuViewController.h"
#import "PhotoViewController.h"


@interface MainViewController () <UITableViewDelegate, UITableViewDataSource,CLLocationManagerDelegate,SWRevealViewControllerDelegate,UIGestureRecognizerDelegate, ErrorsViewDelegate>

@property (nonatomic,retain) CLLocationManager *locationManager;

@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UIView *navigationView;
@property (weak, nonatomic) IBOutlet UILabel *navigationLabel;
@property (weak, nonatomic) IBOutlet UIView *viewWithShadow;
@property (weak, nonatomic) IBOutlet UIButton *myResearchButton;
@property (weak, nonatomic) IBOutlet UIButton *otherResearchButton;
@property (weak, nonatomic) IBOutlet UITableView *researchTableView;
@property (weak, nonatomic) IBOutlet UILabel *myResearchLabel;
@property (weak, nonatomic) IBOutlet UILabel *otherResearchLabel;

@property (weak, nonatomic) IBOutlet UIButton *buttonCopy;

@property (weak, nonatomic) IBOutlet UIView *cordView;
@property (weak, nonatomic) IBOutlet UIView *bckCordView;

@property (weak, nonatomic) IBOutlet UILabel *cordText;

@property NSInteger testUserId;

@property double longtitude;
@property double latitude;

@property (strong, nonatomic) PCAngularActivityIndicatorView *activity;

@property (strong, nonatomic) NSArray *researchArray;

@property (weak, nonatomic) ErrorsView *errorView;
@property (strong, nonatomic) InternetConnection *internetConection;

@property (strong, nonatomic) NSString *bufferString;

@property BOOL geoEnable;


@end


@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    self.internetConection = [[InternetConnection alloc] init];
//    self.toMyResearch = NO;
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    
    self.errorView.delegate = self;
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        [self.locationManager requestAlwaysAuthorization];
    } else {
        [self.locationManager startUpdatingLocation];
    }
    
    CLLocation *location1 = [self.locationManager location];
    CLLocationCoordinate2D coordinate = [location1 coordinate];
    self.longtitude = coordinate.longitude;
    self.latitude = coordinate.latitude;
    NSLog(@"Latitude  = %f", self.latitude);
    NSLog(@"Longitude = %f", self.longtitude);
    
    [self addGradientToButton];
    self.bckCordView.layer.cornerRadius = 8;
    self.bckCordView.clipsToBounds = YES;
    
    
    [self addShadowToView];
    [self.researchTableView setHidden:YES];
    [self.viewWithShadow setHidden:YES];
    
    self.researchArray = [NSArray new];
    
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//    if ([[userDefaults valueForKey:@"isFirst"] boolValue] == YES) {
//        [userDefaults setBool:NO forKey:@"isFirst"];
//        [userDefaults synchronize];
//        self.navigationLabel.text = @"Открытые исследования";
//        [self getResearch];
//        self.isOtherResearch = YES;
//    } else {
//        if (self.isOtherResearch == YES) {
//            self.navigationLabel.text = @"Открытые исследования";
//            [self getResearch];
//        }
//
//        if (self.isMyResearch == YES) {
//            self.navigationLabel.text = @"Мои исследования";
//            [self getActiveResearch];
//        }
//
//        if (self.isMyResearch == NO && self.isOtherResearch == NO) {
//            self.navigationLabel.text = @"Мои исследования";
//            [self getActiveResearch];
//        }
//    }

    self.navigationLabel.text = @"Мої дослідження";
    [self.myResearchButton setSelected:YES];
    [self.myResearchLabel setTextColor:[UIColor whiteColor]];
    
    [self.menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    self.revealViewController.delegate = self;
    self.revealViewController.frontViewController.view.userInteractionEnabled = YES;
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    if ([self.internetConection isOn] == NO) {
        self.errorView = [self createErrorView];
        [self.errorView internetConnectionError];
        [self.view addSubview:self.errorView];
        [self.activity removeFromSuperview];
    }
    
    [self.cordView setHidden:YES];
//    NSString *firstLocation = [NSString stringWithFormat:@"Lat = %f, Lon = %f",self.latitude , self.longtitude];
//    self.cordText.text = [NSString stringWithFormat:@"Ваши кординаты: %@",firstLocation];
//    self.bufferString = self.cordText.text;
    
//    [self getAllReserch];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self addActivityIndicator];
    [self getAllReserch];
}

- (void)addGradientToButton {
    
    self.buttonCopy.layer.cornerRadius = 8;
    self.buttonCopy.clipsToBounds = YES;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = self.buttonCopy.bounds;
    
    UIColor *lightGreen = [UIColor colorWithRed:0.0f/255.0f green:241.0f/255.0f blue:191.0f/255.0f alpha:1];
    UIColor *green = [UIColor colorWithRed:0.0f/255.0f green:182.0f/255.0f blue:153.0f/255.0f alpha:1];
    
    gradient.colors = @[(id)lightGreen.CGColor, (id)green.CGColor];
    
    [self.buttonCopy.layer insertSublayer:gradient atIndex:0];
}

- (void)getAllReserch {
    NSMutableArray *array = [NSMutableArray new];
    [[RequestManager sharedManager] getAllResearchsuccessBlock:^(NSArray *success) {
        for (Research *res in success) {
            [array addObject:res];
        }
        self.researchArray = (NSArray*)array;
        [self.researchTableView reloadData];
        [self.activity removeFromSuperview];
        [self checkArrayFromNil:self.researchArray];
    } withTestId:^(NSInteger testId) {
        self.testUserId = testId;
    } andGeoEneble:^(BOOL geoEnable) {
        self.geoEnable = geoEnable;
    } onFalureBlock:^(NSInteger statusCode) {
        [self checkServerError];
    }];
    
//    //МОИ
//    NSMutableArray *array = [NSMutableArray new];
//    [[RequestManager sharedManager] getActiveResearchsuccessBlock:^(NSArray *success) {
//        for (Research *res in success) {
//            [array addObject:res];
//        }
//        //Открытые
//        [[RequestManager sharedManager] getResearchsuccessBlock:^(NSArray *success) {
//            for (Research *res in success) {
//                [array addObject:res];
//            }
//            self.researchArray = (NSArray*)array;
//            //        self.researchArray = success;
//            [self.researchTableView reloadData];
//            [self.activity removeFromSuperview];
////            self.isMyResearch = NO;
////            self.isOtherResearch = YES;
//            [self checkArrayFromNil:self.researchArray];
//        } onFalureBlock:^(NSInteger statusCode) {
//            [self checkServerError];
//        }];
//        
////        [self.researchTableView reloadData];
////        [self.activity removeFromSuperview];
//        //        self.isMyResearch = YES;
//        //        self.isOtherResearch = NO;
////        [self checkArrayFromNil:self.researchArray];
//    } withTestId:^(NSInteger testId) {
//        self.testUserId = testId;
//    } andGeoEneble:^(BOOL geoEnable) {
//        self.geoEnable = geoEnable;
//    } onFalureBlock:^(NSInteger statusCode) {
//        [self checkServerError];
//    }];
    //Открытые
//    [[RequestManager sharedManager] getResearchsuccessBlock:^(NSArray *success) {
////        self.researchArray = success;
//        [self.researchTableView reloadData];
//        [self.activity removeFromSuperview];
//        self.isMyResearch = NO;
//        self.isOtherResearch = YES;
//        [self checkArrayFromNil:self.researchArray];
//    } onFalureBlock:^(NSInteger statusCode) {
//        [self checkServerError];
//    }];
}

- (void)goToMeResearch {
    [self.errorView removeFromSuperview];
    self.navigationLabel.text = @"Мої дослідження";
    [self getActiveResearch];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MenuViewController * menuVc = [storyboard   instantiateViewControllerWithIdentifier:@"MenuViewController"];
    [menuVc selectMyresearch];
    self.isOtherResearch = NO;
    self.isMyResearch = YES;
    
    
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:4 inSection:0];
//    [menuVc.menuTableView selectRowAtIndexPath:indexPath
//                                      animated:YES
//                                scrollPosition:UITableViewScrollPositionNone];
//    [menuVc.menuTableView reloadData];

}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
    tap = [self.revealViewController tapGestureRecognizer];
    tap.delegate = self;
    if (position == FrontViewPositionRight) {
        [self.view addGestureRecognizer:tap];
    } else {
        [self.view removeGestureRecognizer:tap];
    }
}

//-(void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:YES];
//
////    [self addGradientToView:self.navigationView];
////    [self addGradientToButton:self.myResearchButton];
////    [self addGradientToButton:self.otherResearchButton];
////    [self.researchTableView reloadData];
////    [self viewDidLayoutSubviews];
////    [self.researchTableView layoutSubviews];
//}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self addGradientToView:self.navigationView];
    [self addGradientToButton:self.myResearchButton];
    [self addGradientToButton:self.otherResearchButton];
    
    self.revealViewController.delegate = self;
    
    if (self.toMyResearch == YES) {
        self.navigationLabel.text = @"Мої дослідження";
        [self getActiveResearch];
        self.isMyResearch = YES;
        self.isOtherResearch = NO;
        self.toMyResearch = NO;
    }
    
        [self getAllReserch];

//    [self.researchTableView reloadData];

//    [self.researchTableView layoutSubviews];
}


- (BOOL)checkGeopositionEnable {
    BOOL enable = YES;
    if ([CLLocationManager locationServicesEnabled]){
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            enable = NO;
        }
        else {
            enable = YES;
        }
    }
    return enable;
}

//- (void)viewWillAppear:(BOOL)animated {
//    [self viewWillAppear:YES];
//    self.errorView = [self createErrorView];
//    [self.errorView setHidden:YES];
//}

- (ErrorsView *)createErrorView {
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"ErrorInternetView" owner:self options:nil];
    ErrorsView *errorView = [[ErrorsView alloc] init];
    errorView = [subviewArray objectAtIndex:0];
    errorView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 67, self.view.frame.size.width, self.view.frame.size.height);
//    self.errorView = [self createErrorView];
//    [self.errorView serverError];
//    [self.view addSubview:self.errorView];
    return errorView;
}

- (void)addActivityIndicator {
    self.activity = [[PCAngularActivityIndicatorView alloc] initWithActivityIndicatorStyle:PCAngularActivityIndicatorViewStyleLarge];
    self.activity.center = [self.view convertPoint:self.view.center fromView:self.view.superview];
    self.activity.color = [UIColor colorWithRed:0.0f/255.0f green:182.0f/255.0f blue:153.0f/255.0f alpha:1];
    [self.activity startAnimating];
    [self.view addSubview:self.activity];
}

- (void)checkArrayFromNil:(NSArray *)array {
    self.errorView = [self createErrorView];
    if (self.isOtherResearch == YES) {
        [self.errorView noResearch];
        self.errorView.delegate = self;
    } else if (self.isMyResearch == YES){
        [self.errorView noResearchActive];
        self.errorView.delegate = self;
    } else if (self.isMyResearch == NO && self.isOtherResearch == NO) {
        [self.errorView noResearchActive];
        self.errorView.delegate = self;
    }
    
    if (array.count == 0) {
        [self.researchTableView setHidden:YES];
        [self.errorView setHidden:NO];
        [self.view addSubview:self.errorView];
        self.errorView.delegate = self;

    } else {
        [self.researchTableView setHidden:NO];
        
    }
}
//МОИ
- (void)getActiveResearch {
//    [[RequestManager sharedManager] getActiveResearchsuccessBlock:^(NSArray *success) {
//        self.researchArray = success;
//        [self.researchTableView reloadData];
//        [self.activity removeFromSuperview];
//        [self checkArrayFromNil:self.researchArray];
//    } onFalureBlock:^(NSInteger statusCode) {
//       [self checkServerError];
//    }];
    [[RequestManager sharedManager] getActiveResearchsuccessBlock:^(NSArray *success) {
        self.researchArray = success;
        [self.researchTableView reloadData];
        [self.activity removeFromSuperview];
//        self.isMyResearch = YES;
//        self.isOtherResearch = NO;
        [self checkArrayFromNil:self.researchArray];
    } withTestId:^(NSInteger testId) {
        self.testUserId = testId;
    } andGeoEneble:^(BOOL geoEnable) {
        self.geoEnable = geoEnable;
    } onFalureBlock:^(NSInteger statusCode) {
        [self checkServerError];
    }];
}

- (void)getResearch {
    [[RequestManager sharedManager] getResearchsuccessBlock:^(NSArray *success) {
        self.researchArray = success;
        [self.researchTableView reloadData];
        [self.activity removeFromSuperview];
        self.isMyResearch = NO;
        self.isOtherResearch = YES;
        [self checkArrayFromNil:self.researchArray];
    } onFalureBlock:^(NSInteger statusCode) {
        [self checkServerError];
    }];
}

- (void)checkServerError {
    if ([self.internetConection isOn]) {
        self.errorView = [self createErrorView];
        [self.errorView serverError];
        [self.view addSubview:self.errorView];
        self.errorView.delegate = self;
        
    } else {
        self.errorView = [self createErrorView];
        [self.errorView internetConnectionError];
        [self.view addSubview:self.errorView];
        self.errorView.delegate = self;
        
    }
}
-(IBAction)myReserch:(id)sender {
    [self getActiveResearch];
    [self.myResearchButton setSelected:YES];
    [self.otherResearchButton setSelected:NO];
    [self.myResearchLabel setTextColor:[UIColor whiteColor]];
    [self.otherResearchLabel setTextColor:[UIColor blackColor]];
    
}
-(IBAction)otherResearch:(id)sender {
    [self getResearch];
    [self.myResearchButton setSelected:NO];
    [self.otherResearchButton setSelected:YES];
    [self.otherResearchLabel setTextColor:[UIColor whiteColor]];
    [self.myResearchLabel setTextColor:[UIColor blackColor]];
}

-(IBAction)openCordView:(id)sender {
    [self.cordView setHidden:NO];
}



-(IBAction)cordCopyToBuffer:(id)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    [pasteboard setString:self.bufferString];
}

-(IBAction)updateCord:(id)sender {
    CLLocation *location1 = [self.locationManager location];
    CLLocationCoordinate2D coordinate = [location1 coordinate];
    self.longtitude = coordinate.longitude;
    self.latitude = coordinate.latitude;
    NSLog(@"Latitude  = %f", self.latitude);
    NSLog(@"Longitude = %f", self.longtitude);
    NSString *firstLocation = [NSString stringWithFormat:@"Lat = %f, Lon = %f",self.latitude , self.longtitude];
    self.cordText.text = [NSString stringWithFormat:@"Ваші координати: %@",firstLocation];
}

-(IBAction)openInCard:(id)sender {
    NSString* directionsURL = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%f,%f",self.latitude, self.longtitude];
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: directionsURL] options:@{} completionHandler:^(BOOL success) {}];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: directionsURL]];
    }
}

-(IBAction)closeCordView:(id)sender {
    [self.cordView setHidden:YES];
}

- (void)showConditionsResearch:(id)sender {
    UIButton *button = sender;
    Research *research = self.researchArray[button.tag];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FormsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"FormsViewController"];
    vc.requestString = research.policyURL;
    vc.navigationText = @"Умови дослідження";
    [self.navigationController pushViewController:vc animated:YES];
    NSLog(@"%@", research.name);
    
}


- (UIImage *)imageFromLayer:(CALayer *)layer
{
    UIGraphicsBeginImageContextWithOptions(layer.frame.size, NO, 0);
    
    [layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return outputImage;
}


- (void)addGradientToButton:(UIButton *)button {
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = button.bounds;
    
    UIColor *lightGreen = [UIColor colorWithRed:0.0f/255.0f green:241.0f/255.0f blue:191.0f/255.0f alpha:1];
    UIColor *green = [UIColor colorWithRed:0.0f/255.0f green:182.0f/255.0f blue:153.0f/255.0f alpha:1];
    
    gradient.colors = @[(id)lightGreen.CGColor, (id)green.CGColor];
    
    [button setImage:[self imageFromLayer:gradient] forState:UIControlStateSelected];
}


- (void)addGradientToView:(UIView *)view {
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = view.bounds;
    
    UIColor *lightGreen = [UIColor colorWithRed:0.0f/255.0f green:241.0f/255.0f blue:191.0f/255.0f alpha:1];
    UIColor *green = [UIColor colorWithRed:0.0f/255.0f green:182.0f/255.0f blue:153.0f/255.0f alpha:1];
    
    gradient.colors = @[(id)lightGreen.CGColor, (id)green.CGColor];
    
    [view.layer insertSublayer:gradient atIndex:0];
}


- (BOOL)checkDistanse:(Research *)research {
    User *user = [User currentUser];
    BOOL isCorrect = NO;
    
    if ([research.geoEnable isEqualToNumber:@(0)]) {
        isCorrect = YES;
        return isCorrect;
    }
    
    for (RealmPlace *place in user.place) {
        CLLocation *locA = [[CLLocation alloc] initWithLatitude:self.latitude longitude:self.longtitude];
        CLLocation *locB = [[CLLocation alloc] initWithLatitude:place.latitude longitude:place.longtitude];
        CLLocationDistance distance = [locA distanceFromLocation:locB];
        
        if (distance > 700) {
            isCorrect = NO;
            
            [self.cordView setHidden:NO];
            NSString *firstLocation = [NSString stringWithFormat:@"Lat = %f, Lon = %f",self.latitude , self.longtitude];
            self.cordText.text = [NSString stringWithFormat:@"Ваші координати: %@",firstLocation];
            self.bufferString = self.cordText.text;
            
//            NSString *firstLocation = [NSString stringWithFormat:@"lat:%f, lon:%f",self.latitude , self.longtitude];
//            NSString *secondLocation = [NSString stringWithFormat:@"lat:%f, lon:%f",place.latitude , place.longtitude];
//            self.cordText.text = [NSString stringWithFormat:@"Вы находитесь не на рабочем месте! \n %@ \n %@",firstLocation,secondLocation];
//            self.bufferString = self.cordText.text;
            //bufferString
//            NSString *errorMessage = [NSString stringWithFormat:@"Вы находитесь не на рабочем месте! \n %@ \n %@",firstLocation,secondLocation];
//            [self showAllertViewWithTittle:@"Ошибка" andMessage:errorMessage];
        } else {
            NSLog(@"NORM");
            isCorrect = YES;
            break;
        }
    }
    return isCorrect;
}

- (void)addShadowToView {
    self.viewWithShadow.layer.shadowRadius  = 2;
    self.viewWithShadow.layer.shadowColor   = [UIColor lightGrayColor].CGColor;
    self.viewWithShadow.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    self.viewWithShadow.layer.shadowOpacity = 0.9f;
    self.viewWithShadow.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -2, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(self.viewWithShadow.bounds, shadowInsets)];
    self.viewWithShadow.layer.shadowPath    = shadowPath.CGPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Research *research = self.researchArray[indexPath.row];
    if (self.testUserId == 17 || self.testUserId == 292) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        FormsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"FormsViewController"];
        vc.requestString = research.formURL;
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    
    if ([self checkGeopositionEnable] == YES || self.geoEnable == NO) {
        if (research.isPrivacy == YES) {
            if (self) {
                if ([self checkDistanse:research] == YES) {
//                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                    PhotoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"PhotoViewController"];
//                    vc.requestString = research.formURL;
//                    [self.navigationController pushViewController:vc animated:YES];
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    FormsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"FormsViewController"];
                    vc.requestString = research.formURL;
                    [self.navigationController pushViewController:vc animated:YES];
                } else {
                    [self.cordView setHidden:NO];
                    NSString *firstLocation = [NSString stringWithFormat:@"Lat = %f, Lon = %f",self.latitude , self.longtitude];
                    self.cordText.text = [NSString stringWithFormat:@"Ваші координати: %@",firstLocation];
                    self.bufferString = self.cordText.text;
//                    [self showAllertViewWithTittle:@"Ошибка" andMessage:@"Вы находитесь не на рабочем месте!"];
                    return;
                }
            }
        } else {
            
        }
    } else {
        [self showAllertViewWithTittle:@"Помилка" andMessage:@"Ви заборонили визначати ваше місце розташування для програми Anamnes. Для заповнення анкети ви повинні дозволити доступ, увімкнути GPS та перебувати на своєму робочому місці."];
    }

    if (research.isPrivacy == NO) {
        Research *research = self.researchArray[indexPath.row];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        FormsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"FormsViewController"];
        vc.requestString = research.policyURL;
        vc.navigationText = @"Умови дослідження";
        [self.navigationController pushViewController:vc animated:YES];
        NSLog(@"%@", research.name);
        return;
    }
    
//    if (self.isOtherResearch == YES) {
//        Research *research = self.researchArray[indexPath.row];
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        FormsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"FormsViewController"];
//        vc.requestString = research.policyURL;
//        vc.navigationText = @"Условия исследования";
//        [self.navigationController pushViewController:vc animated:YES];
//        NSLog(@"%@", research.name);
//        return;
//    }
    
//    if ([self checkGeopositionEnable] == YES) {
//        if (self.isOtherResearch == NO) {
//            if (self) {
//                if ([self checkDistanse] == YES) {
//                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                    FormsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"FormsViewController"];
//                    vc.requestString = research.formURL;
//                    [self.navigationController pushViewController:vc animated:YES];
//                } else {
//                    [self showAllertViewWithTittle:@"Ошибка" andMessage:@"Вы находитесь не на рабочем месте!"];
//                    return;
//                }
//            }
//        }
//    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.researchArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ResearchCell *cell = [self.researchTableView dequeueReusableCellWithIdentifier:@"ResearchCell"];
    Research *research = self.researchArray[indexPath.row];
    
    cell.conditionsResearchButton.tag = indexPath.row;
    
    cell.activeLabel.text = [NSString stringWithFormat:@"Активно до %@",research.activeTill];
    cell.researchName.text = research.name;
    [cell.conditionsResearchButton addTarget:self action:@selector(showConditionsResearch:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.backgroundShadowView.layer.cornerRadius = 4;
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: cell.plusView.bounds byRoundingCorners: UIRectCornerTopRight | UIRectCornerBottomRight cornerRadii: (CGSize){4.0, 4.}].CGPath;

    cell.plusView.layer.mask = maskLayer;
    [UITableViewCell addGradientToView:cell.plusView];
    [UITableViewCell addShadowToView:cell.backgroundShadowView];
    [UITableViewCell roundeWithWhiteLayer:cell.activeView];
//    [cell addGradientToView:cell.plusView];
//    [cell addShadowToView];
//    [cell roundeWithWhiteLayer];
    //[[UIScreen mainScreen] bounds]
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    [cell layoutSubviews];
//    [cell layoutIfNeeded] ;
    return cell;
}

- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    UIAlertController *alertController = [UIAlertController new];
    
    alertController = [UIAlertController alertControllerWithTitle:tittle message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:alertController animated:YES completion:NULL];
}

@end
