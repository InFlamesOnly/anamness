//
//  TermsOfUseViewController.m
//  Pigulka
//
//  Created by macOS on 26.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "TermsOfUseViewController.h"
#import "SWRevealViewController.h"
#import "PCAngularActivityIndicatorView.h"
#import "ErrorsView.h"
#import "InternetConnection.h"

@interface TermsOfUseViewController () <UIWebViewDelegate, UIGestureRecognizerDelegate, SWRevealViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *formsWebView;

@property (weak, nonatomic) IBOutlet UIView *submitedView;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UILabel *navigationLabel;
@property (strong, nonatomic) PCAngularActivityIndicatorView *activity;
@property (strong, nonatomic) InternetConnection *internetConnection;

@property (weak, nonatomic) ErrorsView *errorView;

@end

@implementation TermsOfUseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.internetConnection = [[InternetConnection alloc] init];
    
    if ([self.internetConnection isOn] == NO) {
        self.errorView = [self createErrorView];
        [self.errorView internetConnectionError];
        [self.view addSubview:self.errorView];
        [self.activity removeFromSuperview];
        return;
        
    }
    
    [self.submitedView setHidden:YES];
    
    dispatch_queue_t queue = dispatch_queue_create("myqueue", NULL);
    dispatch_async(queue, ^{
        NSString *urlString = @"https://anamnes.com.ua/api/v3/oferta";
        NSURL *url = [NSURL URLWithString:urlString];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
        [self.formsWebView loadRequest:urlRequest];
        
    });
    
    [self.menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
//    UITapGestureRecognizer *tap = [self.revealViewController tapGestureRecognizer];
//    tap.delegate = self;
//    
//    [self.view addGestureRecognizer:tap];
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    self.revealViewController.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated {
    self.revealViewController.delegate = self;
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
    tap = [self.revealViewController tapGestureRecognizer];
    tap.delegate = self;
    if (position == FrontViewPositionRight) {
        
        [self.view addGestureRecognizer:tap];
    } else if (position == FrontViewPositionLeft) {
        [self.view removeGestureRecognizer:tap];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    
    self.activity = [[PCAngularActivityIndicatorView alloc] initWithActivityIndicatorStyle:PCAngularActivityIndicatorViewStyleLarge];
    self.activity.center = [self.view convertPoint:self.view.center fromView:self.view.superview];
    self.activity.color = [UIColor colorWithRed:0.0f/255.0f green:182.0f/255.0f blue:153.0f/255.0f alpha:1];
    [self.activity startAnimating];
    [self.view addSubview:self.activity];
    
    
    
    if ([self.internetConnection isOn] == NO) {
        self.errorView = [self createErrorView];
        [self.errorView internetConnectionError];
        [self.view addSubview:self.errorView];
        [self.activity removeFromSuperview];
    }
    
}

- (ErrorsView *)createErrorView {
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"ErrorInternetView" owner:self options:nil];
    ErrorsView *errorView = [[ErrorsView alloc] init];
    errorView = [subviewArray objectAtIndex:0];
    errorView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 67, self.view.frame.size.width, self.view.frame.size.height);
    //    self.errorView = [self createErrorView];
    //    [self.errorView serverError];
    //    [self.view addSubview:self.errorView];
    return errorView;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.activity removeFromSuperview];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {

    return YES;
}

@end
