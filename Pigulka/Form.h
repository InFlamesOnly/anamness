//
//  Form.h
//  Pigulka
//
//  Created by macOS on 24.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Form : NSObject

@property (nonatomic) NSInteger status;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *date;

- (id)initWithServerResponse:(NSDictionary*)response;

@end
