//
//  BonusesWebViewController.h
//  Pigulka
//
//  Created by macOS on 26.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BonusesWebViewController : UIViewController

@property BOOL isUseBonus;
@property (strong, nonatomic) NSString *bonusURL;

@end
