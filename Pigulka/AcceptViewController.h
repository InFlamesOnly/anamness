//
//  AcceptViewController.h
//  Pigulka
//
//  Created by macOS on 27.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AcceptViewController : UIViewController

@property (strong, nonatomic) NSString *tittleString;
@property (strong, nonatomic) NSString *massageString;

@end
