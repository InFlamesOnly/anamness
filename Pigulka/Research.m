//
//  Research.m
//  Pigulka
//
//  Created by macOS on 14.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "Research.h"

@implementation Research

-(id)initWithServerResponse:(NSDictionary*)response {
    self = [super init];
    if (self) {
        self.researchId = [response valueForKey:@"id"];
        self.name = [response valueForKey:@"name"];
        self.activeTill = [response valueForKey:@"active_till"];
        self.toEnd = [response valueForKey:@"to_end"];
        self.policyURL = [response valueForKey:@"policy_url"];
        self.formURL = [response valueForKey:@"form_url"];
        self.isPrivacy = [[response valueForKey:@"isPrivace"] boolValue];
        
        self.geoEnable = [response valueForKey:@"geo_enable"];
    }
    return self;
}

@end
