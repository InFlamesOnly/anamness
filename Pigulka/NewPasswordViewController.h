//
//  NewPasswordViewController.h
//  Pigulka
//
//  Created by macOS on 05.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewPasswordViewController : UIViewController

@property (strong, nonatomic) NSString *telephoneNumber;
@property (strong, nonatomic) NSString *code;

@property BOOL isProfileViewController;

@end
