//
//  NSLayoutConstraint+UpdateMultiplayer.h
//  Pigulka
//
//  Created by macOS on 13.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSLayoutConstraint (UpdateMultiplayer)

-(instancetype)updateMultiplier:(CGFloat)multiplier;

@end
