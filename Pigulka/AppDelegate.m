//
//  AppDelegate.m
//  Pigulka
//
//  Created by macOS on 03.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "AppDelegate.h"
#import <IQKeyboardManager.h>
#import "User.h"
#import "AutorizationViewController.h"
#import "SWRevealViewController.h"
#import <GooglePlaces/GooglePlaces.h>
#import "RequestManager.h"
#import "RealmPlace.h"
#import "MessageView.h"

@import Firebase;
@import UserNotifications;

#define SYSTEM_VERSION_LESS_THAN(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface AppDelegate () <CLLocationManagerDelegate, UNUserNotificationCenterDelegate, FIRMessagingDelegate>

@property (strong, nonatomic) User *user;
@property (strong, nonatomic) CLLocationManager *locationManager;

@property (strong, nonatomic) UIView *messageView;
@property (strong, nonatomic) NSDictionary *userInfo;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [self updSchema];
    [GMSPlacesClient provideAPIKey:@"AIzaSyCGuMKCwjmNQkyQdK0q4d4cBsLAtPMrlII"];
    [self checkFirstScreen];
    [self checkLocationServicesAndStartUpdates];
     [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
#endif
    }
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    [FIRApp configure];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [[IQKeyboardManager sharedManager] setEnable:YES];
    
    return YES;
}

- (void) updSchema {
    // Inside your [AppDelegate didFinishLaunchingWithOptions:]

    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
    // Set the new schema version. This must be greater than the previously used
    // version (if you've never set a schema version before, the version is 0).
    config.schemaVersion = 4;

    // Set the block which will be called automatically when opening a Realm with a
    // schema version lower than the one set above
    config.migrationBlock = ^(RLMMigration *migration, uint64_t oldSchemaVersion) {
        // We haven’t migrated anything yet, so oldSchemaVersion == 0
        if (oldSchemaVersion < 1) {
            // Nothing to do!
            // Realm will automatically detect new properties and removed properties
            // And will update the schema on disk automatically
        }
    };

    // Tell Realm to use this new configuration object for the default Realm
    [RLMRealmConfiguration setDefaultConfiguration:config];

    // Now that we've told Realm how to handle the schema change, opening the file
    // will automatically perform the migration
    [RLMRealm defaultRealm];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive)
    {
        //When your app is in foreground and it received a push notification
    }
    else if (state == UIApplicationStateInactive)
    {
        //When your app was in background and it received a push notification
    }
    NSLog(@"%@", userInfo);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    NSLog(@"MESSAGE!!!!!!!!!!!!!!!!!!!!!!!!!!");
    self.userInfo = userInfo;
    if ( application.applicationState == UIApplicationStateActive ) {
        
    } else if ( application.applicationState == UIApplicationStateBackground ) {
        [NSTimer scheduledTimerWithTimeInterval:2.5
                                         target:self
                                       selector:@selector(showMessage)
                                       userInfo:nil
                                        repeats:NO];
        
//        UIViewController *vc = [self topViewController];
        
//        MessageView *view = [[MessageView alloc] init];;
//        view = [view loadFromXib];
//        view.frame = CGRectMake(0, 64, vc.view.frame.size.width, vc.view.frame.size.height - 64 - 200);
//        view.message.text = [[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] valueForKey:@"body"];
//        [vc.view addSubview:view];
//
//        [self addShadowToView:view];
    } else if ( application.applicationState == UIApplicationStateInactive ) {
        [NSTimer scheduledTimerWithTimeInterval:2.5
                                         target:self
                                       selector:@selector(showMessage)
                                       userInfo:nil
                                        repeats:NO];
//        UIViewController *vc = [self topViewController];
//
//        MessageView *view = [[MessageView alloc] init];;
//        view = [view loadFromXib];
//        view.frame = CGRectMake(0, 64, vc.view.frame.size.width, vc.view.frame.size.height - 64 - 200);
//        view.message.text = [[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] valueForKey:@"body"];
//        [vc.view addSubview:view];
//        [self addShadowToView:view];
    }
    
}

- (void)showMessage {
    UIViewController *vc = [self topViewController];
    
    MessageView *view = [[MessageView alloc] init];;
    view = [view loadFromXib];
    view.frame = CGRectMake(0, 64, vc.view.frame.size.width, vc.view.frame.size.height - 64 - 200);
    view.message.text = [[[self.userInfo valueForKey:@"aps"] valueForKey:@"alert"] valueForKey:@"body"];
    [vc.view addSubview:view];
    
    [self addShadowToView:view];
}

- (void)userNotificationCenter:(UNUserNotificationCenter* )center willPresentNotification:(UNNotification* )notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
        UIViewController *vc = [self topViewController];
//        MessageView *mesView = [[MessageView alloc]init];
        
        MessageView *view = [[MessageView alloc] init];;
        view = [view loadFromXib];
        view.frame = CGRectMake(0, 64, vc.view.frame.size.width, vc.view.frame.size.height - 64 - 200);
        view.message.text = [notification.request.content valueForKey:@"body"];
//        view.center = vc.view.center;
        [vc.view addSubview:view];
        [self addShadowToView:view];
        
    }
    NSLog(@"");
    completionHandler(UNNotificationPresentationOptionAlert);
    NSLog(@"Userinfo %@",notification.request.content.userInfo);
}

- (void)addShadowToView:(UIView*)view {
    view.layer.shadowRadius  = 2;
    [view.layer setShadowColor: [UIColor grayColor].CGColor];
    [view.layer setShadowOpacity:0.8];
    [view.layer setShadowRadius:3.0];
    [view.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    
    // For iOS 10 display notification (sent via APNS)
    [UNUserNotificationCenter currentNotificationCenter].delegate = self;
    // For iOS 10 data message (sent via FCM)
    [FIRMessaging messaging].delegate = self;
    
    [application registerForRemoteNotifications];
    
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [[FIRMessaging messaging] setAPNSToken:deviceToken type:0];
    [[FIRMessaging messaging] setAPNSToken:deviceToken type:2];
    [[FIRMessaging messaging] setAPNSToken:deviceToken type:1];
    
}

- (void)messaging:(nonnull FIRMessaging *)messaging didRefreshRegistrationToken:(nonnull NSString *)fcmToken {
    User *currentUser = [User currentUser];
    if (currentUser != nil) {
        [[RequestManager sharedManager] sendToken:fcmToken SuccesBlock:^(NSDictionary *success) {
            
        } onFalureBlock:^(NSInteger statusCode) {
            
        }];
    }
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    
//    if (![self.user.token isEqualToString:@""]) {
//        [[RequestManager sharedManager] updateUserFCMToken:fcmToken withSuccessBlock:^(NSDictionary *success) {
//
//        } onFalureBlock:^(NSInteger statusCode) {
//
//        }];
//    }
    
    //    [[RequestManager sharedManager] registerNotificationWithFCMToken:fcmToken successBlock:^(NSDictionary *success) {
    //
    //    } onFalureBlock:^(NSInteger statusCode) {
    //
    //    }];
    
    NSLog(@"FCM registration token: %@", fcmToken);
    
    // TODO: If necessary send token to application server.
}

- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
    
    User *currentUser = [User currentUser];
    if (currentUser != nil) {
        [[RequestManager sharedManager] sendToken:refreshedToken SuccesBlock:^(NSDictionary *success) {
            
        } onFalureBlock:^(NSInteger statusCode) {
            
        }];
    }
    
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    
    // TODO: If necessary send token to application server.
}

- (void)connectToFcm {
    // Won't connect since there is no token
    if (![[FIRInstanceID instanceID] token]) {
        return;
    }
    
    // Disconnect previous FCM connection if it exists.
    [[FIRMessaging messaging] shouldEstablishDirectChannel];
    
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM. FCM token - %@", [[FIRInstanceID instanceID] token] );
        }
    }];
}

-(void) checkLocationServicesAndStartUpdates
{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    //Checking authorization status
    if (![CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Помилка"
                                                            message:@"Ви заборонили визначати ваше місце розташування для програми Anamnes. Для заповнення анкети ви повинні дозволити доступ, увімкнути GPS та перебувати на своєму робочому місці."
                                                           delegate:self
                                                  cancelButtonTitle:@"Налаштування"
                                                  otherButtonTitles:@"ОК", nil];
        
        //TODO if user has not given permission to device
        if (![CLLocationManager locationServicesEnabled])
        {
            alertView.tag = 100;
        }
        //TODO if user has not given permission to particular app
        else
        {
            alertView.tag = 200;
        }
        
        [alertView show];
        
        return;
    }
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex == 0)//Settings button pressed
    {
        if (alertView.tag == 100)
        {
            
            //This will open ios devices location settings
//            NSString* url = SYSTEM_VERSION_LESS_THAN(@"10.0") ? @"prefs:root=LOCATION_SERVICES" : @"App-Prefs:root=Privacy&path=LOCATION";
             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
        else if (alertView.tag == 200)
        {
            //This will opne particular app location settings
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
    }
    else if(buttonIndex == 1)//Cancel button pressed.
    {
        //TODO for cancel
    }
}

- (BOOL)checkUserLogin {
    if ([User allObjects].count == 0) {
        return NO;
    } else {
        self.user = [User currentUser];
        if (self.user.token) {
            [self getBonusesAndCurrentPlace];
            return YES;
        }
        else {
            return NO;
        }
    }
}


- (void)getBonusesAndCurrentPlace {
    [[RequestManager sharedManager] getBonusWithPlaceSuccesBlock:^(NSDictionary *success) {
        User *newUserData = [[User alloc] init];
        newUserData.bonuses = [success valueForKey:@"bonuses"];
        for (NSDictionary *dic in [success valueForKey:@"places"]) {
            RealmPlace *place = [[RealmPlace alloc] init];
            place.name = [dic valueForKey:@"name"];
            place.address = [dic valueForKey:@"address"];
            place.longtitude = [[dic valueForKey:@"longitude"] doubleValue];
            place.latitude = [[dic valueForKey:@"latitude"] doubleValue];
            [newUserData.place addObject:place];
        }
        [User updateUser:newUserData];
        
    } onFalureBlock:^(NSInteger statusCode) {
        
    }];
}

- (void)checkFirstScreen {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *rootViewController;
    
    if ([self checkUserLogin]) {
        rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
        self.window.rootViewController = rootViewController;
        [self.window makeKeyAndVisible];
    } else {
        rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"AutorizationViewController"];
        self.window.rootViewController = rootViewController;
        [self.window makeKeyAndVisible];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (UIViewController *)topViewController{
    return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self topViewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}


@end
