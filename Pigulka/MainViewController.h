//
//  MainViewController.h
//  Pigulka
//
//  Created by macOS on 10.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController

@property BOOL isOtherResearch;
@property BOOL isMyResearch;
@property BOOL toMyResearch;

@end
