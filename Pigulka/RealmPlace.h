//
//  RealmPlace.h
//  Pigulka
//
//  Created by macOS on 26.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <Realm/Realm.h>

@interface RealmPlace : RLMObject

@property NSString *name;
@property NSString *address;
@property double latitude;
@property double longtitude;

@end

RLM_ARRAY_TYPE(RealmPlace)
