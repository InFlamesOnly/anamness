//
//  Place.m
//  Pigulka
//
//  Created by macOS on 24.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "Place.h"

@implementation Place

- (id)initWithServerResponse:(NSDictionary*)response {
    self = [super init];
    if (self) {
        self.placeId = [[response valueForKey:@"id"] integerValue];
        self.status = [[response valueForKey:@"status"] integerValue];
        self.name = [response valueForKey:@"name"];
        self.longitude = [[response valueForKey:@"longitude"] doubleValue];
        self.latitude = [[response valueForKey:@"latitude"] doubleValue];
        self.address = [response valueForKey:@"address"];
    }
    return self;
}


@end
