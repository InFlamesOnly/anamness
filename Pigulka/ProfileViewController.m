//
//  ProfileViewController.m
//  Pigulka
//
//  Created by macOS on 12.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "ProfileViewController.h"
#import "RoundedViewWithGreenBorder.h"
#import "SWRevealViewController.h"
#import "WorkCell.h"
#import "ChangeWorkAddressViewController.h"
#import "RequestManager.h"
#import "UserFromPlases.h"
#import "Place.h"
#import "ChangeTelephoneViewController.h"
#import "ForgotPasswordViewController.h"
#import "PCAngularActivityIndicatorView.h"
#import "ErrorsView.h"
#import "InternetConnection.h"
#import "BonusesViewController.h"
#import "AccCell.h"
#import "AccInfo.h"
#import "User.h"
#import "MainViewController.h"

@interface ProfileViewController () <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, SWRevealViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UIButton *addNewWorkButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *showOrHideWorkConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *parentViewConstraint;
@property (weak, nonatomic) IBOutlet UITableView *workTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;



@property (weak, nonatomic) IBOutlet UILabel *profileLabel;
@property (weak, nonatomic) IBOutlet UILabel *ballsLabel;
@property (weak, nonatomic) IBOutlet UILabel *telephoneLabel;
@property (weak, nonatomic) IBOutlet UITextField *proffesionTextField;
@property (weak, nonatomic) IBOutlet UITextField *positionTextField;
@property (weak, nonatomic) IBOutlet UITextField *expirianseTextField;

@property (strong, nonatomic) UserFromPlases *user;
@property (strong, nonatomic) PCAngularActivityIndicatorView *activity;
@property (strong, nonatomic) InternetConnection *internetConnection;

@property (weak, nonatomic) ErrorsView *errorView;

@property (weak, nonatomic) IBOutlet UIButton *changeAccountButton;

@property (weak, nonatomic) IBOutlet UITableView *accTableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *accTableViewHeightConstr;

@property (strong, nonatomic) NSArray *accInfo;

@property (weak, nonatomic) IBOutlet UIImageView *buttonOpenImage;


@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.changeAccountButton setSelected:NO];
    self.accInfo = [NSArray new];
    [self.buttonOpenImage setImage:[UIImage imageNamed:@"OpenBtn"]];
}

- (void)setButtonInfoFromToken {
    User *currentUser = [User currentUser];
    for (AccInfo *info in self.accInfo) {
        if ([currentUser.token isEqualToString:info.token]) {
            if ([info.name isEqualToString:@""]) {
                [self.changeAccountButton setTitle:info.phone forState:UIControlStateNormal];
            } else {
                [self.changeAccountButton setTitle:info.name forState:UIControlStateNormal];
            }
        }
    }
}

-(void)viewWillAppear:(BOOL)animated {
    [self addActivityIndicator];
    [self.scrollView setHidden:YES];
    
    self.internetConnection = [[InternetConnection alloc] init];
    
    [self getInfo];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTapped)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.ballsLabel addGestureRecognizer:tapGestureRecognizer];
    self.ballsLabel.userInteractionEnabled = YES;
    tapGestureRecognizer.cancelsTouchesInView = NO;
    [self.menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    UITapGestureRecognizer *dissmissTap = [self.revealViewController tapGestureRecognizer];
    dissmissTap.delegate = self;
    dissmissTap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:dissmissTap];
    
    if ([self.internetConnection isOn] == NO) {
        self.errorView = [self createErrorView];
        [self.errorView internetConnectionError];
        [self.view addSubview:self.errorView];
        [self.activity removeFromSuperview];
    }
    self.revealViewController.delegate = self;
//    self.revealViewController.delegate = self;
}

- (void)getInfo {
    [[RequestManager sharedManager] getMyProfileSuccessBlock:^(UserFromPlases *success) {
        self.user = success;
        self.profileLabel.text = [NSString stringWithFormat:@"%@ %@, у вас",self.user.name, self.user.middleName];
        self.ballsLabel.text = self.user.bonuses;
        self.proffesionTextField.text = self.user.profession;
        self.positionTextField.text = self.user.position;
        self.expirianseTextField.text = self.user.experience;
        self.telephoneLabel.text = self.user.phone;
        
        if (self.user.plases.count == 0) {
            [self zeroWork];
            [self.workTableView reloadData];
        }
        if (self.user.plases.count == 1) {
            [self oneWork];
            [self.workTableView reloadData];
        }
        if (self.user.plases.count >= 2) {
            [self twoWork];
            [self.workTableView reloadData];
        }
        
        [[RequestManager sharedManager] getAccounts:^(NSDictionary *success) {
            NSLog(@"%@",success);
            NSMutableArray *array = [NSMutableArray new];
            for (NSDictionary *dic in success) {
                AccInfo *info = [[AccInfo alloc] initWithServerResponse:dic];
                [array addObject:info];
            }
            self.accInfo = array;
            [self.accTableView reloadData];
            
//            [self setButtonInfoFromToken];
            [self.activity removeFromSuperview];
            [self.scrollView setHidden:NO];
        } onFalureBlock:^(NSInteger statusCode) {
            [self.activity removeFromSuperview];
            [self.scrollView setHidden:NO];
            [self checkServerError];
        }];
    } onFalureBlock:^(NSInteger statusCode) {
        [self.activity removeFromSuperview];
        [self.scrollView setHidden:NO];
        [self checkServerError];
        
    }];
}

///MainViewController

- (IBAction)openAccauntTableView:(id)sender {
    [self.changeAccountButton setSelected:!self.changeAccountButton.isSelected];
    if (self.changeAccountButton.isSelected) {
        [self openWithCellCount:self.accInfo.count];
        [self.buttonOpenImage setImage:[UIImage imageNamed:@"CloseBtn"]];
//        [self.buttonOpenImage setImage:[UIImage imageNamed:@"OpenBtn"]];
        
        //openTableView
    } else {
        [self close];
        [self.buttonOpenImage setImage:[UIImage imageNamed:@"OpenBtn"]];
//        [self.buttonOpenImage setImage:[UIImage imageNamed:@"CloseBtn"]];
        //closeTableView
    }
}

- (void)openWithCellCount:(int)count {
    self.accTableViewHeightConstr.constant = count * 44;
    CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height;
    if (screenHeight <= self.accTableViewHeightConstr.constant) {
        self.accTableViewHeightConstr.constant = screenHeight - 150;
    }
    [UIView animateWithDuration:0.1 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)close {
    self.accTableViewHeightConstr.constant = 0;
    [UIView animateWithDuration:0.1 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
    tap = [self.revealViewController tapGestureRecognizer];
    tap.delegate = self;
    if (position == FrontViewPositionRight) {
        
        [self.view addGestureRecognizer:tap];
    } else if (position == FrontViewPositionLeft) {
        [self.view removeGestureRecognizer:tap];
    }
}

- (void)labelTapped {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BonusesViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BonusesViewController"];
    [self.navigationController pushViewController:vc animated:YES];

}

- (void)checkServerError {
    if ([self.internetConnection isOn]) {
        self.errorView = [self createErrorView];
        [self.errorView serverError];
        [self.view addSubview:self.errorView];
    } else {
        self.errorView = [self createErrorView];
        [self.errorView internetConnectionError];
        [self.view addSubview:self.errorView];
    }
}

- (ErrorsView *)createErrorView {
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"ErrorInternetView" owner:self options:nil];
    ErrorsView *errorView = [[ErrorsView alloc] init];
    errorView = [subviewArray objectAtIndex:0];
    errorView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 67, self.view.frame.size.width, self.view.frame.size.height);
    //    self.errorView = [self createErrorView];
    //    [self.errorView serverError];
    //    [self.view addSubview:self.errorView];
    return errorView;
}

- (void)addActivityIndicator {
    self.activity = [[PCAngularActivityIndicatorView alloc] initWithActivityIndicatorStyle:PCAngularActivityIndicatorViewStyleLarge];
    self.activity.center = [self.view convertPoint:self.view.center fromView:self.view.superview];
    self.activity.color = [UIColor colorWithRed:0.0f/255.0f green:182.0f/255.0f blue:153.0f/255.0f alpha:1];
    [self.activity startAnimating];
    [self.view addSubview:self.activity];
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];

}

- (void)changeWork:(id)sender {
    UIButton *button = sender;
    Place *place = self.user.plases[button.tag];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ChangeWorkAddressViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ChangeWorkAddressViewController"];
    vc.place = place;
    NSLog(@"%ld", (long)place.placeId);
    [self.navigationController pushViewController:vc animated:YES];    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"isNewAddress"])
    {
        ChangeWorkAddressViewController *vc = [segue destinationViewController];
        vc.isNewAddress = YES;
    }
    if ([[segue identifier] isEqualToString:@"changeTelephoneNumber"])
    {
        ChangeTelephoneViewController *vc = [segue destinationViewController];
        vc.oldTelephone = self.user.phone;
    }

}

- (IBAction)save:(id)sender {
    [[RequestManager sharedManager] saveProfileWithProfession:self.proffesionTextField.text experiance:self.expirianseTextField.text andPosition:self.positionTextField.text succesBlock:^(NSInteger success) {
        if (success == 200) {
            [self showAllertViewWithTittle:@"Успех" andMessage:@"Данные изменены!"];
        }
    } onFalureBlock:^(NSInteger statusCode) {
        
    }];
}

- (IBAction)changePassword:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ForgotPasswordViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordViewController"];
    vc.isProfileViewController = YES;
    [self.navigationController pushViewController:vc animated:YES];

}

//changeOrRemoveAddress
- (void)oneWork {
    self.showOrHideWorkConstraint.constant = 190;
    self.parentViewConstraint.constant = self.parentViewConstraint.constant + 160;
    self.tableViewHeightConstraint.constant = 135;
    [self.workTableView setHidden:NO];
}

- (void)twoWork {
    self.showOrHideWorkConstraint.constant = 190 + 60;
    self.parentViewConstraint.constant = self.parentViewConstraint.constant + 160 + 85;
    self.tableViewHeightConstraint.constant = 135 + 135;
    [self.addNewWorkButton setHidden:YES];
    [self.workTableView setHidden:NO];
    

}

- (void)zeroWork {
    self.showOrHideWorkConstraint.constant = 22;
    self.parentViewConstraint.constant = self.parentViewConstraint.constant;
    [self.workTableView setHidden:YES];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}


-(void)dismissKeyboard {
    [self.proffesionTextField resignFirstResponder];
    [self.positionTextField resignFirstResponder];
    [self.expirianseTextField resignFirstResponder];
    
}

- (void)getBonusesAndCurrentPlace {
    [self addActivityIndicator];
    [[RequestManager sharedManager] getBonusWithPlaceSuccesBlock:^(NSDictionary *success) {
        User *newUserData = [[User alloc] init];
        newUserData.bonuses = [success valueForKey:@"bonuses"];
        for (NSDictionary *dic in [success valueForKey:@"places"]) {
            RealmPlace *place = [[RealmPlace alloc] init];
            place.name = [dic valueForKey:@"name"];
            place.address = [dic valueForKey:@"address"];
            place.longtitude = [[dic valueForKey:@"longitude"] doubleValue];
            place.latitude = [[dic valueForKey:@"latitude"] doubleValue];
            [newUserData.place addObject:place];
        }
        [User updateUser:newUserData];
        
        [self getInfo];
        
    } onFalureBlock:^(NSInteger statusCode) {
        [self.activity removeFromSuperview];
        [self.scrollView setHidden:NO];
    }];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    User *currentUser = [User currentUser];
    if (tableView == self.accTableView) {
        AccInfo *info = self.accInfo[indexPath.row];
        if (![currentUser.token isEqualToString:info.token]) {
            [[RLMRealm defaultRealm] beginWriteTransaction];
            currentUser.token = info.token;
            [[RLMRealm defaultRealm] commitWriteTransaction];
            [self getBonusesAndCurrentPlace];
            [self close];
            [self.buttonOpenImage setImage:[UIImage imageNamed:@"OpenBtn"]];
            [self goToMainViewController];
        }
    }
    
    [self close];
}

- (void)goToMainViewController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MainViewController *log = [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
    [self.navigationController pushViewController:log animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.accTableView) {
        return self.accInfo.count;
    }
    
    if (self.user.plases.count >= 2) {
        return 2;
    } else {
        return self.user.plases.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.accTableView) {
        AccCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AccCell" forIndexPath:indexPath];
        AccInfo *info = self.accInfo[indexPath.row];

        if ([info.name isEqualToString:@""]) {
            cell.value.text = info.phone;
        } else {
            cell.value.text = info.name;
        }
        
        if ([info.token isEqualToString:[User currentUser].token]) {
            [cell.acceptImage setHidden:NO];
        } else {
            [cell.acceptImage setHidden:YES];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    WorkCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WorkCell"];
    Place *place = self.user.plases[indexPath.row];
    cell.name.text = place.name;
    cell.address.text = place.address;
    cell.changeWork.tag = indexPath.row;
    [cell.changeWork addTarget:self action:@selector(changeWork:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    UIAlertView *templateAlertView = [[UIAlertView alloc] initWithTitle:tittle
                                                                message:message
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil, nil];
    templateAlertView.alertViewStyle = UIAlertViewStyleDefault;
    [templateAlertView show];
}


@end
