//
//  SupportViewController.m
//  Pigulka
//
//  Created by macOS on 12.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "SupportViewController.h"
#import "SWRevealViewController.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "User.h"

@interface SupportViewController () <MFMailComposeViewControllerDelegate, UIGestureRecognizerDelegate, SWRevealViewControllerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UIView *navigationView;



@end


@implementation SupportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//    self.revealViewController.delegate = self;
//    UITapGestureRecognizer *tap = [self.revealViewController tapGestureRecognizer];
//    tap.delegate = self;
//
//    [self.view addGestureRecognizer:tap];
    
}

-(void)viewWillAppear:(BOOL)animated {
    self.revealViewController.delegate = self;
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
    tap = [self.revealViewController tapGestureRecognizer];
    tap.delegate = self;
    if (position == FrontViewPositionRight) {
        
        [self.view addGestureRecognizer:tap];
    } else if (position == FrontViewPositionLeft) {
        [self.view removeGestureRecognizer:tap];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return 2;
//}
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    AccCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AccCell" forIndexPath:indexPath];
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    return cell;
//}

-(IBAction)callNumber:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:380937100288"]];
}
-(IBAction)callNumber2:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:380950143453"]];
}
-(IBAction)callNumber3:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:380974913880"]];
}
-(IBAction)sendMessage:(id)sender {
    User *user = [User currentUser];
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    NSString *mail = @"help@anamnes.com.ua";
    [controller setToRecipients:[NSArray arrayWithObject:mail]];
    NSString *subject = [NSString stringWithFormat:@"Звернення до служби підтримки %@ %@",user.name,user.middleName];
    [controller setSubject:subject];
//    [controller setMessageBody:@"Hello there." isHTML:NO];
    if (controller) [self presentModalViewController:controller animated:YES];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissModalViewControllerAnimated:YES];
}

@end
