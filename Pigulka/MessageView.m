//
//  MessageView.m
//  Pigulka
//
//  Created by macOS on 05.06.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "MessageView.h"

@implementation MessageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}

- (MessageView*)loadFromXib {
    NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:@"MessageView"
                                                      owner:self
                                                    options:nil];
    
    MessageView *mainView = (MessageView*)[nibViews objectAtIndex:0];
    mainView.backroundView.layer.cornerRadius = 15;
    mainView.backroundView.layer.masksToBounds = YES;
    mainView.closeButton.layer.cornerRadius = mainView.closeButton.frame.size.height/2;
    mainView.closeButton.layer.masksToBounds = YES;
    return mainView;
}
- (IBAction)close:(id)sender {
    [self removeFromSuperview];
}

@end
