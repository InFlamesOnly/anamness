//
//  Form.m
//  Pigulka
//
//  Created by macOS on 24.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "Form.h"

@implementation Form

- (id)initWithServerResponse:(NSDictionary*)response {
    self = [super init];
    if (self) {
        self.status = [[response valueForKey:@"status"] integerValue];
        self.name = [response valueForKey:@"name"];
        self.text = [response valueForKey:@"text"];
        self.date = [response valueForKey:@"date"];
    }
    return self;
}

@end
