//
//  BonuseCell.m
//  Pigulka
//
//  Created by macOS on 26.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "BonuseCell.h"

@implementation BonuseCell

- (void)awakeFromNib {
    [super awakeFromNib];
//    [self addShadowToView];
//    [self roundeWithWhiteLayer];
    self.backgroundShadowView.layer.cornerRadius = 4;
}

- (void)addGradientToView:(UIView *)view {
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = view.bounds;
    
    UIColor *lightGreen = [UIColor colorWithRed:0.0f/255.0f green:241.0f/255.0f blue:191.0f/255.0f alpha:1];
    UIColor *green = [UIColor colorWithRed:0.0f/255.0f green:182.0f/255.0f blue:153.0f/255.0f alpha:1];
    
    gradient.colors = @[(id)lightGreen.CGColor, (id)green.CGColor];
    
    [view.layer insertSublayer:gradient atIndex:0];
}

- (void)addShadowToView {
//    self.backgroundShadowView.layer.shadowRadius  = 2;
//    //    self.backgroundShadowView.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
//    self.backgroundShadowView.layer.shadowColor   = [UIColor lightGrayColor].CGColor;
//    self.backgroundShadowView.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
//    self.backgroundShadowView.layer.shadowOpacity = 0.9f;
//    self.backgroundShadowView.layer.masksToBounds = NO;
//    
//    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -2, 0);
//    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(self.backgroundShadowView.bounds, shadowInsets)];
//    self.backgroundShadowView.layer.shadowPath    = shadowPath.CGPath;
//    [self layoutSubviews];
    self.backgroundShadowView.layer.shadowRadius  = 2;
    //    self.backgroundShadowView.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    self.backgroundShadowView.layer.shadowColor   = [UIColor lightGrayColor].CGColor;
    self.backgroundShadowView.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    self.backgroundShadowView.layer.shadowOpacity = 0.9f;
    self.backgroundShadowView.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, 0, 0);
    CGRect shadowBounds = CGRectMake(0, 0, self.backgroundShadowView.frame.size.width - 10, self.backgroundShadowView.frame.size.height + 2);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(shadowBounds, shadowInsets)];
    self.backgroundShadowView.layer.shadowPath    = shadowPath.CGPath;
}

-(void)roundeWithWhiteLayer {
    self.activeView.layer.cornerRadius = self.activeView.bounds.size.width/2;
    self.activeView.layer.borderWidth = 1.0;
    self.activeView.layer.borderColor = [[UIColor grayColor] CGColor];
}

//-(void)addShadowToActiveView {
//    self.activeView.layer.shadowColor = [UIColor colorWithRed:209/255 green:220/255 blue:228/255 alpha:0.2].CGColor;
//    self.activeView.layer.shadowOffset = CGSizeMake(0, 1);
//    self.activeView.layer.shadowOpacity = 1;
//    self.activeView.layer.shadowRadius = 4;
//}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
