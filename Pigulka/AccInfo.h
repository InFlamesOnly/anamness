//
//  AccInfo.h
//  Pigulka
//
//  Created by macOS on 15.11.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AccInfo : NSObject

@property (strong, nonatomic) NSString *accId;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *token;

- (id)initWithServerResponse:(NSDictionary*)response;

@end

NS_ASSUME_NONNULL_END
