//
//  NewPasswordViewController.m
//  Pigulka
//
//  Created by macOS on 05.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "NewPasswordViewController.h"
#import "RoundedViewWithGreenBorder.h"
#import "RequestManager.h"
#import "AutorizationViewController.h"
#import "ProfileViewController.h"
#import "NavigationView.h"
#import "SWRevealViewController.h"

@interface NewPasswordViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *navigationLabel;
@property (weak, nonatomic) IBOutlet UILabel *logoLabel;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet RoundedViewWithGreenBorder *passwordView;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet NavigationView *navigationView;
@property (weak, nonatomic) IBOutlet UIView *backView;

@end


@implementation NewPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.backView.layer.cornerRadius = self.backView.frame.size.width / 2;
    self.backView.clipsToBounds = YES;
    [self addGradientToView:self.backView];
    [self.navigationView setHidden:YES];
    [self.logoLabel setHidden:NO];
    
    
    if (self.isProfileViewController == YES) {
        [self.backView setHidden:YES];
        [self.navigationView setHidden:NO];
        [self.logoLabel setHidden:YES];
    }
    
    [self addIntervalToLogo];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

- (void)addGradientToView:(UIView *)view {
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = view.bounds;
    
    UIColor *lightGreen = [UIColor colorWithRed:0.0f/255.0f green:241.0f/255.0f blue:191.0f/255.0f alpha:1];
    UIColor *green = [UIColor colorWithRed:0.0f/255.0f green:182.0f/255.0f blue:153.0f/255.0f alpha:1];
    
    gradient.colors = @[(id)lightGreen.CGColor, (id)green.CGColor];
    
    [view.layer insertSublayer:gradient atIndex:0];
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)addIntervalToLogo {
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"Anamnesis"];
    [attributedString addAttribute:NSKernAttributeName
                             value:@(2)
                             range:NSMakeRange(0, 9)];
    
    self.logoLabel.attributedText = attributedString;
}

- (IBAction)resetPassword:(id)sender {
    if (self.passwordTextField.text.length == 0) {
        [self showAllertViewWithTittle:@"Помилка" andMessage:@"Поле пароль пусте"];
    } else {
        if (self.passwordTextField.text.length < 4) {
            [self showAllertViewWithTittle:@"Помилка" andMessage:@"Пароль повинен складатися не менше ніж із 4 символів"];
            return;
        } else {
            [[RequestManager sharedManager] resetOldPasswordFromNew:self.passwordTextField.text withCode:self.code andPhoneNumber:self.telephoneNumber successBlock:^(NSDictionary *success) {
                [self showAllertViewWithTittleWithAction:@"Ура" andMessage:@"Пароль успішно змінено"];
            } onFalureBlock:^(NSInteger statusCode) {
       
            }];
        }
    }
}

-(void)dismissKeyboard {
    [self.passwordView borderOff];
    [self.passwordTextField resignFirstResponder];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == self.passwordTextField) {
        [self.passwordView borderOn];
    }
    return YES;
}

- (void)goToAutorisationViewController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AutorizationViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"AutorizationViewController"];
    [self.navigationController pushViewController:vc animated:YES];

}

- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    UIAlertController *alertController = [UIAlertController new];
    
    alertController = [UIAlertController alertControllerWithTitle:tittle message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:alertController animated:YES completion:NULL];
}

- (void)showAllertViewWithTittleWithAction:(NSString*) tittle andMessage:(NSString*) message {
    UIAlertController *alertController = [UIAlertController new];
    
    alertController = [UIAlertController alertControllerWithTitle:tittle message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        if (self.isProfileViewController == YES) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            ProfileViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
            [self.navigationController pushViewController:vc animated:YES];

        } else {
        
            [self goToAutorisationViewController];
        }
    }]];
    [self presentViewController:alertController animated:YES completion:NULL];
}


@end
