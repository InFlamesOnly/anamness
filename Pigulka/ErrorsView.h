//
//  ErrorsView.h
//  Pigulka
//
//  Created by macOS on 27.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ButtonWithGradient.h"

@protocol ErrorsViewDelegate <NSObject>
@optional
- (void)goToMeResearch;
@end


@interface ErrorsView : UIView

@property (nonatomic,retain) id <ErrorsViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet ButtonWithGradient *toMyResearchButton;
@property (weak, nonatomic) IBOutlet UIImageView *errorImage;
@property (weak, nonatomic) IBOutlet UILabel *errorMassage;
@property (weak, nonatomic) IBOutlet UILabel *errorTittle;

- (void)internetConnectionError;
- (void)serverError;
- (void)noResearch;
- (void)noResearchActive;
- (void)changeWork;

@end
