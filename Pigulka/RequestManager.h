//
//  RequestManager.h
//  Pigulka
//
//  Created by macOS on 06.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserFromPlases.h"

@interface RequestManager : NSObject

+ (RequestManager*)sharedManager;

- (void)autorisationUserFromTelephoneNumber:(NSString*)telephoneNumber
                                andPassword:(NSString*)password
                               successBlock:(void(^)(NSDictionary*success)) success
                              onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)forgotPassword:(NSString*)telephoneNumber
          successBlock:(void(^)(NSDictionary*success)) success
         onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)checkCodeFromServer:(NSString*)code
                  withPhone:(NSString*)telephoneNumber
               successBlock:(void(^)(NSDictionary*success)) success
              onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)resetOldPasswordFromNew:(NSString*)newPassword
                       withCode:(NSString*)code
                 andPhoneNumber:(NSString*)telephoneNumber
                   successBlock:(void(^)(NSDictionary*success)) success
                  onFalureBlock:(void(^)(NSInteger statusCode)) fail;
//- (void)getActiveResearchsuccessBlock:(void(^)(NSArray *success)) success
//                        onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)getResearchsuccessBlock:(void(^)(NSArray *success)) success
                  onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)getUserWorkSheetsWithBonusessText:(void(^)(NSString *text)) text
                                companyId:(void(^)(NSNumber *companyId)) companyId
                                bonuesURL:(void(^)(NSString *bonusesURL)) bonusesURL
                          howUseBonuesURL:(void(^)(NSString *bonusesURL)) howUseBonues
                              sucessBlock:(void(^)(NSArray *success)) success
                            onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)getMyProfileSuccessBlock:(void(^)(UserFromPlases *success)) success
                   onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)chengeTelephoneNumber:(NSString *)telephoneNumber
                  succesBlock:(void(^)(NSInteger success)) success
                onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)saveProfileWithProfession:(NSString*)profession
                       experiance:(NSString*)experience
                      andPosition:(NSString*)position
                      succesBlock:(void(^)(NSInteger success)) success
                    onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)addNewWork:(NSString *)work
       withAddress:(NSString*)address
        longtitude:(double)longtitude
          latitude:(double)latitude
       succesBlock:(void(^)(NSInteger success)) success
     onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)getBonusWithPlaceSuccesBlock:(void(^)(NSDictionary *success)) success
                       onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)getActiveResearchsuccessBlock:(void(^)(NSArray *success)) success
                           withTestId:(void(^)(NSInteger testId)) testId
                         andGeoEneble:(void(^)(BOOL geoEnable)) geoEnable
                        onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)changeWork:(NSString *)work
       withAddress:(NSString*)address
           placeId:(NSNumber*)placeId
        longtitude:(double)longtitude
          latitude:(double)latitude
            status:(int)status
       succesBlock:(void(^)(NSInteger success)) success
     onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)getAccounts:(void(^)(NSDictionary *success)) success
      onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)getAllResearchsuccessBlock:(void(^)(NSArray *success)) success
                        withTestId:(void(^)(NSInteger testId)) testId
                      andGeoEneble:(void(^)(BOOL geoEnable)) geoEnable
                     onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)sendToken:(NSString*)fcmtoken
      SuccesBlock:(void(^)(NSDictionary *success)) success
    onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)getHistoryList:(void(^)(NSString *text)) text
         onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)getHistoryBonus:(void(^)(NSString *text)) text
          onFalureBlock:(void(^)(NSInteger statusCode)) fail;

@end
