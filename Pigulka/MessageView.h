//
//  MessageView.h
//  Pigulka
//
//  Created by macOS on 05.06.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageView : UIView

@property (weak, nonatomic) IBOutlet UIView *backroundView;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UILabel *message;

- (MessageView*)loadFromXib;

@end
