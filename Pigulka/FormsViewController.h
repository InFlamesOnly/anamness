//
//  ViewController.h
//  Pigulka
//
//  Created by macOS on 03.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FormsViewController : UIViewController

@property (strong, nonatomic) NSString *requestString;
@property (strong, nonatomic) NSString *navigationText;

@end

