//
//  Place.h
//  Pigulka
//
//  Created by macOS on 24.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Place : NSObject

@property (nonatomic) NSInteger placeId;
@property (nonatomic) NSInteger status;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *address;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;

- (id)initWithServerResponse:(NSDictionary*)response;


@end
