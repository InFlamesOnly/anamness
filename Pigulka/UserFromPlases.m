//
//  UserFromPlases.m
//  Pigulka
//
//  Created by macOS on 25.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "UserFromPlases.h"
#import "Place.h"

@implementation UserFromPlases

-(id)initWithServerResponse:(NSDictionary*)response {
    self = [super init];
    if (self) {
        self.name = [response valueForKey:@"name"];
        self.surname = [response valueForKey:@"surname"];
        self.middleName = [response valueForKey:@"middle_name"];
        self.region = [response valueForKey:@"region"];
        self.city = [response valueForKey:@"city"];
        self.profession = [response valueForKey:@"profession"];
        self.experience = [response valueForKey:@"experience"];
        self.position = [response valueForKey:@"position"];
        self.bonuses = [response valueForKey:@"bonuses"];
        self.phone = [response valueForKey:@"phone"];
        
        NSMutableArray *array = [NSMutableArray new];
        
        for (NSDictionary *dic in [response valueForKey:@"places"]) {
            Place *place = [[Place alloc] initWithServerResponse:dic];
            [array addObject:place];
        }
        
        self.plases = array;
        
    }
    
    return self;
}

@end
