//
//  SplashScreenViewController.m
//  Pigulka
//
//  Created by macOS on 27.03.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "SplashScreenViewController.h"
#import "User.h"
#import "RequestManager.h"

@interface SplashScreenViewController ()

@end

@implementation SplashScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [NSTimer scheduledTimerWithTimeInterval:2.0
                                     target:self
                                   selector:@selector(checkFirstScreen)
                                   userInfo:nil
                                    repeats:NO];
}

- (void)checkFirstScreen {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *rootViewController;
    
    if ([self checkUserLogin]) {
//        [self performSegueWithIdentifier:@"main" sender:self];
        rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
        [UIApplication sharedApplication].delegate.window.rootViewController = rootViewController;
        [[UIApplication sharedApplication].delegate.window makeKeyAndVisible];
    } else {
//        [self performSegueWithIdentifier:@"login" sender:self];
        rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"AutorizationViewController"];
        [UIApplication sharedApplication].delegate.window.rootViewController = rootViewController;
        [[UIApplication sharedApplication].delegate.window makeKeyAndVisible];
    }
}

- (BOOL)checkUserLogin {
    User *currentUser = [[User alloc] init];
    if ([User allObjects].count == 0) {
        return NO;
    } else {
        currentUser = [User currentUser];
        if (currentUser.token) {
            [self getBonusesAndCurrentPlace];
            return YES;
        }
        else {
            return NO;
        }
    }
}

- (void)getBonusesAndCurrentPlace {
    [[RequestManager sharedManager] getBonusWithPlaceSuccesBlock:^(NSDictionary *success) {
        User *newUserData = [[User alloc] init];
        newUserData.bonuses = [success valueForKey:@"bonuses"];
        for (NSDictionary *dic in [success valueForKey:@"places"]) {
            RealmPlace *place = [[RealmPlace alloc] init];
            place.name = [dic valueForKey:@"name"];
            place.address = [dic valueForKey:@"address"];
            place.longtitude = [[dic valueForKey:@"longitude"] doubleValue];
            place.latitude = [[dic valueForKey:@"latitude"] doubleValue];
            [newUserData.place addObject:place];
        }
        [User updateUser:newUserData];
        
    } onFalureBlock:^(NSInteger statusCode) {
        
    }];
}

@end
