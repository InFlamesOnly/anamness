//
//  ForgotPasswordViewController.m
//  Pigulka
//
//  Created by macOS on 05.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "RoundedViewWithGreenBorder.h"
#import "ButtonWithGradient.h"
#import "NewPasswordViewController.h"
#import "RequestManager.h"
#import "TelephoneNumberValidator.h"
#import "NSString+PhoneNumber.h"
#import "NavigationView.h"
#import "SWRevealViewController.h"

@interface ForgotPasswordViewController () <UIGestureRecognizerDelegate, SWRevealViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet NavigationView *navigationView;
@property (weak, nonatomic) IBOutlet UILabel *logoLabel;
@property (weak, nonatomic) IBOutlet UITextField *telephoneTextField;
@property (weak, nonatomic) IBOutlet RoundedViewWithGreenBorder *telephoneView;
@property (weak, nonatomic) IBOutlet RoundedViewWithGreenBorder *messageView;
@property (weak, nonatomic) IBOutlet UITextField *messageTextField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sendSmsConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *messageIcon;
@property (weak, nonatomic) IBOutlet ButtonWithGradient *getSmsButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hiddenViewConstraint;
@property (weak, nonatomic) IBOutlet UIView *hiddenView;

@property (weak, nonatomic) IBOutlet UIView *backView;

@property BOOL userTapToGetCode;


@end


@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationView setHidden:YES];
    [self.logoLabel setHidden:NO];
    self.userTapToGetCode = NO;
    self.backView.layer.cornerRadius = self.backView.frame.size.width / 2;
    self.backView.clipsToBounds = YES;
    [self addGradientToView:self.backView];
    
    if (self.isProfileViewController == YES) {
        [self.navigationView setHidden:NO];
        [self.backView setHidden:YES];
        [self.logoLabel setHidden:YES];
        [self.menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        self.revealViewController.delegate = self;
//        UITapGestureRecognizer *dissmissTap = [self.revealViewController tapGestureRecognizer];
//        dissmissTap.delegate = self;
//        
//        [self.view addGestureRecognizer:dissmissTap];
    }
    
    [self hideMessageView];
    [self addIntervalToLogo];
    self.sendSmsConstraint.constant = 20;
    self.hiddenViewConstraint.constant = 0;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

- (void)addGradientToView:(UIView *)view {
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = view.bounds;
    
    UIColor *lightGreen = [UIColor colorWithRed:0.0f/255.0f green:241.0f/255.0f blue:191.0f/255.0f alpha:1];
    UIColor *green = [UIColor colorWithRed:0.0f/255.0f green:182.0f/255.0f blue:153.0f/255.0f alpha:1];
    
    gradient.colors = @[(id)lightGreen.CGColor, (id)green.CGColor];
    
    [view.layer insertSublayer:gradient atIndex:0];
}

-(void)viewWillAppear:(BOOL)animated {
    if (self.isProfileViewController == YES) {
        self.revealViewController.delegate = self;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
    tap = [self.revealViewController tapGestureRecognizer];
    tap.delegate = self;
    if (position == FrontViewPositionRight) {
        
        [self.view addGestureRecognizer:tap];
    } else if (position == FrontViewPositionLeft) {
        [self.view removeGestureRecognizer:tap];
    }
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)hideMessageView {
    [self.hiddenView setHidden:YES];
    [self.messageIcon setHidden:YES];
    [self.messageView setHidden:YES];
    [self.messageTextField setHidden:YES];
}

- (void)showMessageView {
    [self.hiddenView setHidden:NO];
    [self.messageIcon setHidden:NO];
    [self.messageView setHidden:NO];
    [self.messageTextField setHidden:NO];
}

- (void)addIntervalToLogo {
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"Anamnesis"];
    [attributedString addAttribute:NSKernAttributeName
                             value:@(2)
                             range:NSMakeRange(0, 9)];
    
    self.logoLabel.attributedText = attributedString;
}

- (IBAction)sendSmsOrGetNewPassword:(id)sender {
    NSString *buttonTittle = self.getSmsButton.titleLabel.text;
    if (self.userTapToGetCode == NO) {
        [self getCodeFromServer];
        return;
    }
    
    if (self.userTapToGetCode == YES) {
        [self checkCodeFromServer];
        return;
    }
//    self.userTapToGetCode = NO;
//    if ([buttonTittle isEqualToString:@"Получить смс"]) {
//
//    }
//    if ([buttonTittle isEqualToString:@"Продолжить"]) {
//        [self checkCodeFromServer];
//    }
}

- (void)checkCodeFromServer {
    if (self.messageTextField.text.length == 0) {
        [self showAllertViewWithTittle:@"Помилка" andMessage:@"Поле код пусте"];
    } else {
        if (self.messageTextField.text.length < 6) {
            [self showAllertViewWithTittle:@"Помилка" andMessage:@"Поле код має мати 6 цифр"];
            return;
        } else {
            NSString *phoneNumber = [NSString removeAllCharacteFromValidatePhoneNumber:self.telephoneTextField.text];
            [[RequestManager sharedManager] checkCodeFromServer:self.messageTextField.text withPhone:phoneNumber successBlock:^(NSDictionary *success) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                NewPasswordViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewPasswordViewController"];
                vc.isProfileViewController = self.isProfileViewController;
                vc.telephoneNumber = phoneNumber;
                vc.code = self.messageTextField.text;
                [self.navigationController pushViewController:vc animated:YES];
            } onFalureBlock:^(NSInteger statusCode) {
                
            }];
        }
    }
}

- (void)getCodeFromServer {
    if (self.telephoneTextField.text.length == 0) {
        [self showAllertViewWithTittle:@"Помилка" andMessage:@"Поле номер телефону пусте!"];
        return;
    } else {
        NSString *phoneNumber = [NSString removeAllCharacteFromValidatePhoneNumber:self.telephoneTextField.text];
        if (phoneNumber.length == 13) {
            [[RequestManager sharedManager] forgotPassword:phoneNumber successBlock:^(NSDictionary *success) {
                self.userTapToGetCode = YES;
                [self openCodeView];
            } onFalureBlock:^(NSInteger statusCode) {
                
            }];
        } else {
            [self showAllertViewWithTittle:@"Помилка" andMessage:@"Вкажіть валідний номер!"];
        }
    }
}


- (void)openCodeView {
    [self showMessageView];
    [UIView animateWithDuration:2 animations:^{
        self.sendSmsConstraint.constant = 87;
        self.hiddenViewConstraint.constant = 59;
        [self.getSmsButton setTitle:@"Продовжити" forState:UIControlStateNormal];
    }];
}

-(void)dismissKeyboard {
    [self.telephoneView borderOff];
    [self.messageView borderOff];
    [self.telephoneTextField resignFirstResponder];
    [self.messageTextField resignFirstResponder];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == self.telephoneTextField) {
        [self.telephoneView borderOn];
        [self.messageView borderOff];
    } else if (textField == self.telephoneTextField) {
        [self.telephoneView borderOff];
        [self.messageView borderOn];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.telephoneTextField) {
        if (textField.text.length == 0) {
            textField.text = @"+38 ";
        }
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.telephoneTextField) {
        if (range.location < 19) {
            return [TelephoneNumberValidator formattedTextField:textField shouldChangeCharactersInRange:range replacementString:string];
        } else {
            return NO;
        }
    }
    
    if (textField == self.messageTextField){
        if (range.location < 6) {
            return YES;
        }
        else {
           return NO;
        }
    }
    else {
        return YES;
    }
}

- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    UIAlertController *alertController = [UIAlertController new];
    
    alertController = [UIAlertController alertControllerWithTitle:tittle message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:alertController animated:YES completion:NULL];
}

@end
