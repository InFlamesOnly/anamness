//
//  ChangeTelephoneViewController.m
//  Pigulka
//
//  Created by macOS on 13.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "ChangeTelephoneViewController.h"
#import "TelephoneNumberValidator.h"
#import "NSString+PhoneNumber.h"
#import "SWRevealViewController.h"
#import "RequestManager.h"
#import "ErrorsView.h"
#import "InternetConnection.h"
#import "AcceptViewController.h"

@interface ChangeTelephoneViewController () <UITextFieldDelegate, UIGestureRecognizerDelegate, SWRevealViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *telephoneTextField;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;

@property (strong, nonatomic) InternetConnection *internetConnection;

@property (weak, nonatomic) ErrorsView *errorView;

@end


@implementation ChangeTelephoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.internetConnection = [[InternetConnection alloc] init];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    UITapGestureRecognizer *dissmisDrawer = [self.revealViewController tapGestureRecognizer];
    tap.delegate = self;
    
    [self.view addGestureRecognizer:dissmisDrawer];
    
    [self.menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    
//    UITapGestureRecognizer *dissmissTap = [self.revealViewController tapGestureRecognizer];
//    tap.delegate = self;
//    
//    [self.view addGestureRecognizer:dissmissTap];
    
    
    if ([self.internetConnection isOn] == NO) {
        self.errorView = [self createErrorView];
        [self.errorView internetConnectionError];
        [self.view addSubview:self.errorView];
    }
//    self.revealViewController.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated {
    self.revealViewController.delegate = self;
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
    tap = [self.revealViewController tapGestureRecognizer];
    tap.delegate = self;
    if (position == FrontViewPositionRight) {
        
        [self.view addGestureRecognizer:tap];
    } else if (position == FrontViewPositionLeft) {
        [self.view removeGestureRecognizer:tap];
    }
}

- (void)checkServerError {
    if ([self.internetConnection isOn]) {
        self.errorView = [self createErrorView];
        [self.errorView serverError];
        [self.view addSubview:self.errorView];
    } else {
        self.errorView = [self createErrorView];
        [self.errorView internetConnectionError];
        [self.view addSubview:self.errorView];
    }
}

- (ErrorsView *)createErrorView {
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"ErrorInternetView" owner:self options:nil];
    ErrorsView *errorView = [[ErrorsView alloc] init];
    errorView = [subviewArray objectAtIndex:0];
    errorView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 67, self.view.frame.size.width, self.view.frame.size.height);
    //    self.errorView = [self createErrorView];
    //    [self.errorView serverError];
    //    [self.view addSubview:self.errorView];
    return errorView;
}

-(IBAction)send:(id)sender {
    [self changePawwordRequest];
}

- (void)dismissKeyboard {
    [self.telephoneTextField resignFirstResponder];
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.telephoneTextField) {
        if (textField.text.length == 0) {
            textField.text = @"+38 ";
        }
    }
}

- (void)changePawwordRequest {
    NSString *phoneNumber = [NSString removeAllCharacteFromValidatePhoneNumber:self.telephoneTextField.text];
    if ([self.oldTelephone isEqualToString:phoneNumber]) {
        [self showAllertViewWithTittle:@"Помилка" andMessage:@"Номер збігається із старим!"];
        return;
    }
    if (phoneNumber.length == 13) {
        [[RequestManager sharedManager] chengeTelephoneNumber:phoneNumber succesBlock:^(NSInteger success) {
            if (success == 200) {
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                AcceptViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"AcceptViewController"];
                vc.tittleString = @"Вашу заявку на зміну номера телефону відправлено!";
                [self.navigationController pushViewController:vc animated:YES];
//                [self showAllertViewWithTittle:@"Успех" andMessage:@"Номер телефона успешно изменен"];
            }
            
        } onFalureBlock:^(NSInteger statusCode) {
            [self checkServerError];
        }];
    } else {
        [self showAllertViewWithTittle:@"Помилка" andMessage:@"Вкажіть валідний номер!"];
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.telephoneTextField) {
        if (range.location < 19) {
            return [TelephoneNumberValidator formattedTextField:textField shouldChangeCharactersInRange:range replacementString:string];
        } else {
            return NO;
        }
    }
    else {
        return YES;
    }
}

#pragma mark - AllertView
- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    UIAlertController *alertController = [UIAlertController new];
    
    alertController = [UIAlertController alertControllerWithTitle:tittle message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:alertController animated:YES completion:NULL];
}

@end
