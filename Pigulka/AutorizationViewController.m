//
//  AutorizationViewController.m
//  Pigulka
//
//  Created by macOS on 05.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "AutorizationViewController.h"
#import "RoundedViewWithGreenBorder.h"
#import "ButtonWithGradient.h"
#import "RequestManager.h"
#import "FormsViewController.h"
#import "TelephoneNumberValidator.h"
#import "NSString+PhoneNumber.h"
#import "SWRevealViewController.h"
#import "User.h"
#import "PCAngularActivityIndicatorView.h"

@import Firebase;

@interface AutorizationViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *logoLabel;
@property (weak, nonatomic) IBOutlet RoundedViewWithGreenBorder *telephoneView;
@property (weak, nonatomic) IBOutlet RoundedViewWithGreenBorder *passwordView;
@property (weak, nonatomic) IBOutlet UITextField *telephoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet ButtonWithGradient *logInButton;
@property (weak, nonatomic) IBOutlet UIButton *forgotPassword;
@property (strong, nonatomic) PCAngularActivityIndicatorView *activity;
@property (strong, nonatomic) UIView *backgroundView;

@end


@implementation AutorizationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addUnderlineToButton];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    NSArray *array = [UIApplication sharedApplication].delegate.window.rootViewController.childViewControllers;
    NSLog(@"%@",array);
    self.forgotPassword.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    [self addIntervalToLogo];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

- (void)removeActiveity {
    [self.activity removeFromSuperview];
    [self.backgroundView removeFromSuperview];
}
- (void)addActivityIndicator {
    self.activity = [[PCAngularActivityIndicatorView alloc] initWithActivityIndicatorStyle:PCAngularActivityIndicatorViewStyleLarge];
    self.backgroundView = [UIView new];
    self.backgroundView.frame = self.view.frame;
    self.backgroundView.backgroundColor = [UIColor blackColor];
    self.backgroundView.alpha = 0.7;
    self.activity.center = [self.view convertPoint:self.view.center fromView:self.view.superview];
    self.activity.color = [UIColor colorWithRed:0.0f/255.0f green:182.0f/255.0f blue:153.0f/255.0f alpha:1];
    [self.activity startAnimating];
    [self.backgroundView addSubview:self.activity];
    [self.view addSubview:self.backgroundView];
}

//[self addActivityIndicator];

- (IBAction)autorisationUser:(id)sender {
    if (self.telephoneTextField.text.length == 0) {
        [self showAllertViewWithTittle:@"Помилка" andMessage:@"Поле номер телефону пусте!"];
        return;
    } else if (self.passwordTextField.text.length == 0) {
        [self showAllertViewWithTittle:@"Помилка" andMessage:@"Поле пароль пусте!"];
        return;
    } else {
        [self autorisationRequest];
    }
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

//- (void)checkFirstScreen {
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UIViewController *rootViewController;
//    rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
//    [UIApplication sharedApplication].delegate.window.rootViewController = rootViewController;
//    [[UIApplication sharedApplication].delegate.window makeKeyAndVisible];
//}

- (void)autorisationRequest {
    [self addActivityIndicator];
    NSString *phoneNumber = [NSString removeAllCharacteFromValidatePhoneNumber:self.telephoneTextField.text];
    if (phoneNumber.length == 13) {
        [[RequestManager sharedManager] autorisationUserFromTelephoneNumber:phoneNumber andPassword:self.passwordTextField.text successBlock:^(NSDictionary *success) {
            if ([[success valueForKey:@"statusCode"] isEqualToString:@"error"]) {
                [self removeActiveity];
                return;
            } else {
                User *user = [[User alloc] initWithServerResponse:success];
                if (user) {
                    [User saveUser:user];
                    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
                    [[RequestManager sharedManager] sendToken:refreshedToken SuccesBlock:nil onFalureBlock:nil];
                    [self getBonusesAndCurrentPlace];
            }
        }
            
        } onFalureBlock:^(NSInteger statusCode) {
            [self removeActiveity];
            [self showAllertViewWithTittle:@"Помилка" andMessage:@"Проблема з Інтернетом"];
            
        }];
    } else {
        [self removeActiveity];
        [self showAllertViewWithTittle:@"Помилка" andMessage:@"Вкажіть валідний номер телефону!"];
    }

}

- (void)getBonusesAndCurrentPlace {
    [[RequestManager sharedManager] getBonusWithPlaceSuccesBlock:^(NSDictionary *success) {
        User *newUserData = [[User alloc] init];
        newUserData.bonuses = [success valueForKey:@"bonuses"];
        for (NSDictionary *dic in [success valueForKey:@"places"]) {
            RealmPlace *place = [[RealmPlace alloc] init];
            place.name = [dic valueForKey:@"name"];
            place.address = [dic valueForKey:@"address"];
            place.longtitude = [[dic valueForKey:@"longitude"] doubleValue];
            place.latitude = [[dic valueForKey:@"latitude"] doubleValue];
            [newUserData.place addObject:place];
        }
        [User updateUser:newUserData];
        [self removeActiveity];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *rootViewController;
        rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
        [UIApplication sharedApplication].delegate.window.rootViewController = rootViewController;
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setBool:YES forKey:@"isFirst"];
        [userDefaults synchronize];
//        [[UIApplication sharedApplication].delegate.window makeKeyAndVisible];
        [self performSegueWithIdentifier:@"login" sender:self];
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        SWRevealViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
//        [self.navigationController pushViewController:vc animated:YES];

    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActiveity];
    }];
}


#pragma mark - CustomizeUI
- (void)addUnderlineToButton {
    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:@"Новий пароль"];
    UIColor *green = [UIColor colorWithRed:0.0f/255.0f green:182.0f/255.0f blue:153.0f/255.0f alpha:1];
    [titleString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
    [titleString addAttribute:NSForegroundColorAttributeName value:green range:NSMakeRange(0, [titleString length])];
    [self.forgotPassword setAttributedTitle: titleString forState:UIControlStateNormal];
}

- (void)addIntervalToLogo {
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"Anamnesis"];
    [attributedString addAttribute:NSKernAttributeName
                             value:@(2)
                             range:NSMakeRange(0, 9)];
    
    self.logoLabel.attributedText = attributedString;
}

#pragma mark - UITextFields
-(void)dismissKeyboard {
    [self.passwordView borderOff];
    [self.telephoneView borderOff];
    [self.telephoneTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == self.telephoneTextField) {
        [self.telephoneView borderOn];
        [self.passwordView borderOff];
    } else if (textField == self.passwordTextField) {
        [self.passwordView borderOn];
        [self.telephoneView borderOff];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.telephoneTextField) {
        if (textField.text.length == 0) {
            textField.text = @"+38 ";
        }
    }

}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.telephoneTextField) {
        if (range.location < 19) {
            return [TelephoneNumberValidator formattedTextField:textField shouldChangeCharactersInRange:range replacementString:string];
        } else {
            return NO;
        }
    }
    else {
        return YES;
    }
}

#pragma mark - AllertView
- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    UIAlertController *alertController = [UIAlertController new];
    
    alertController = [UIAlertController alertControllerWithTitle:tittle message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:alertController animated:YES completion:NULL];
}

@end
