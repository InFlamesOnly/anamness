//
//  ChangeWorkAddressViewController.h
//  Pigulka
//
//  Created by macOS on 13.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Place.h"

@interface ChangeWorkAddressViewController : UIViewController

@property BOOL isNewAddress;
@property (strong, nonatomic) Place *place;

@end
