//
//  AcceptViewController.m
//  Pigulka
//
//  Created by macOS on 27.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "AcceptViewController.h"
#import "SWRevealViewController.h"

@interface AcceptViewController () <SWRevealViewControllerDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UILabel *tittleLabel;
@property (weak, nonatomic) IBOutlet UILabel *massageLabel;

@end

@implementation AcceptViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    self.tittleLabel.text = self.tittleString;
    self.massageLabel.text = self.massageString;
}

-(void)viewWillAppear:(BOOL)animated {
    self.revealViewController.delegate = self;
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
    tap = [self.revealViewController tapGestureRecognizer];
    tap.delegate = self;
    if (position == FrontViewPositionRight) {
        
        [self.view addGestureRecognizer:tap];
    } else if (position == FrontViewPositionLeft) {
        [self.view removeGestureRecognizer:tap];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (IBAction)back:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
