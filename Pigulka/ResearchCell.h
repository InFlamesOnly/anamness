//
//  ResearchCell.h
//  Pigulka
//
//  Created by macOS on 10.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResearchCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *activeView;
@property (weak, nonatomic) IBOutlet UILabel *activeLabel;
@property (weak, nonatomic) IBOutlet UILabel *researchName;
@property (weak, nonatomic) IBOutlet UIButton *conditionsResearchButton;
@property (weak, nonatomic) IBOutlet UIView *plusView;
@property (weak, nonatomic) IBOutlet UIView *backgroundShadowView;

- (void)addShadowToView;
- (void)roundeWithWhiteLayer;
- (void)addGradientToView:(UIView *)view;
- (void)layoutSubviews;

@end
