//
//  RoundedViewWithGreenBorder.m
//  Pigulka
//
//  Created by macOS on 05.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "RoundedViewWithGreenBorder.h"
#import "Constants.h"


@implementation RoundedViewWithGreenBorder

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self addRoundedBorder];
        
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self addRoundedBorder];
    }
    return self;
}

- (void) addRoundedBorder {
    self.layer.cornerRadius = 8;
    self.clipsToBounds = YES;
    self.layer.borderWidth = 1;
    self.layer.borderColor = [UIColor colorWithRed:251.0f/255.0f green:250.0f/255.0f blue:255.0f/255.0f alpha:1].CGColor;
    self.backgroundColor = [UIColor colorWithRed:251.0f/255.0f green:250.0f/255.0f blue:255.0f/255.0f alpha:1];
}

- (void) borderOn {
    self.layer.borderColor = [UIColor colorWithRed:0.0f/255.0f green:191.0f/255.0f blue:158.0f/255.0f alpha:1].CGColor;
    self.backgroundColor = [UIColor colorWithRed:232.0f/255.0f green:255.0f/255.0f blue:250.0f/255.0f alpha:1];
}

- (void) borderOff {
    self.layer.borderColor = [UIColor colorWithRed:251.0f/255.0f green:250.0f/255.0f blue:255.0f/255.0f alpha:1].CGColor;
    self.backgroundColor = [UIColor colorWithRed:251.0f/255.0f green:250.0f/255.0f blue:255.0f/255.0f alpha:1];
}

@end
