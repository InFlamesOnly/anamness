//
//  ButtonWithGradient.m
//  Pigulka
//
//  Created by macOS on 05.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "ButtonWithGradient.h"

@implementation ButtonWithGradient

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self addGradientToButton];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self addGradientToButton];
    }
    return self;
}

- (void)addGradientToButton {
    
    self.layer.cornerRadius = 8;
    self.clipsToBounds = YES;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = self.bounds;
    
    UIColor *lightGreen = [UIColor colorWithRed:0.0f/255.0f green:241.0f/255.0f blue:191.0f/255.0f alpha:1];
    UIColor *green = [UIColor colorWithRed:0.0f/255.0f green:182.0f/255.0f blue:153.0f/255.0f alpha:1];
    
    gradient.colors = @[(id)lightGreen.CGColor, (id)green.CGColor];
    
    [self.layer insertSublayer:gradient atIndex:0];
}

@end
