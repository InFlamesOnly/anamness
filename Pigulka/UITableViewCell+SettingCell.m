//
//  UITableViewCell+SettingCell.m
//  Pigulka
//
//  Created by macOS on 18.10.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "UITableViewCell+SettingCell.h"

@implementation UITableViewCell (SettingCell)

+ (void)addGradientToView:(UIView *)view {
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = view.bounds;
//    gradient.frame = CGRectMake(view.frame.origin.x, view.frame.origin.x, view.frame.size.width - 1, view.frame.size.height - 1);

    
    UIColor *lightGreen = [UIColor colorWithRed:0.0f/255.0f green:241.0f/255.0f blue:191.0f/255.0f alpha:1];
    UIColor *green = [UIColor colorWithRed:0.0f/255.0f green:182.0f/255.0f blue:153.0f/255.0f alpha:1];
    
    gradient.colors = @[(id)lightGreen.CGColor, (id)green.CGColor];
    
    [view.layer insertSublayer:gradient atIndex:0];
}

+ (void)addShadowToView:(UIView*)view {
    view.layer.shadowRadius  = 2;
//    //    self.backgroundShadowView.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
//    view.layer.shadowColor   = [UIColor lightGrayColor].CGColor;
//    view.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
//    view.layer.shadowOpacity = 0.9f;
////    view.layer.masksToBounds = NO;
////
//    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -0.5, 0);
//    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(view.bounds, shadowInsets)];
//    view.layer.shadowPath    = shadowPath.CGPath;
//    view.layer.shadowColor   = [UIColor lightGrayColor].CGColor;
//    view.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
//    view.layer.shadowOpacity = 1;
//    view.layer.shadowRadius = 1.0;
//    view.layer.shadowRadius  = 1.5f;
//    view.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
//    view.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
//    view.layer.shadowOpacity = 0.9f;
//    view.layer.masksToBounds = NO;
    [view.layer setShadowColor: [UIColor grayColor].CGColor];
    [view.layer setShadowOpacity:0.8];
    [view.layer setShadowRadius:3.0];
    [view.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
//    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
//    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(view.bounds, shadowInsets)];
//    view.layer.shadowPath    = shadowPath.CGPath;
}

+ (void)roundeWithWhiteLayer:(UIView*)view {
    view.layer.cornerRadius = view.bounds.size.width/2;
    view.layer.borderWidth = 1.0;
    view.layer.borderColor = [[UIColor grayColor] CGColor];
}

@end
