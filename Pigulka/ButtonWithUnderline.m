//
//  ButtonWithUnderline.m
//  Pigulka
//
//  Created by macOS on 12.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "ButtonWithUnderline.h"

@implementation ButtonWithUnderline

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self addUnderlineToButton];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self addUnderlineToButton];
    }
    return self;
}

- (void)addUnderlineToButton {
    if (self.titleLabel.text != nil) {
        NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:self.titleLabel.text];
        UIColor *green = [UIColor colorWithRed:0.0f/255.0f green:182.0f/255.0f blue:153.0f/255.0f alpha:1];
        [titleString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
        [titleString addAttribute:NSForegroundColorAttributeName value:green range:NSMakeRange(0, [titleString length])];
        [self setAttributedTitle: titleString forState:UIControlStateNormal];
    }
}

@end
