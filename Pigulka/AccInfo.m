//
//  AccInfo.m
//  Pigulka
//
//  Created by macOS on 15.11.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "AccInfo.h"

@implementation AccInfo

-(id)initWithServerResponse:(NSDictionary*)response {
    self = [super init];
    if (self) {
        self.accId = [response valueForKey:@"id"];
        self.phone = [response valueForKey:@"phone"];
        
        if ([response valueForKey:@"name"] == (id)[NSNull null]) {
            self.name = @"";
        } else {
            self.name = [response valueForKey:@"name"];
        }
    
        self.token = [response valueForKey:@"token"];
    }
    return self;
}

@end
