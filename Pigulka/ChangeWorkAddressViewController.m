//
//  ChangeWorkAddressViewController.m
//  Pigulka
//
//  Created by macOS on 13.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "ChangeWorkAddressViewController.h"
#import "AddressCell.h"
#import <IQKeyboardManager.h>
#import "NSLayoutConstraint+UpdateMultiplayer.h"
#import "SWRevealViewController.h"
#import "RequestManager.h"
#import "ErrorsView.h"
#import "InternetConnection.h"
#import "AcceptViewController.h"
#import "User.h"

@import GooglePlaces;

@interface ChangeWorkAddressViewController () <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate, CLLocationManagerDelegate, SWRevealViewControllerDelegate>

@property (nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSMutableArray *placesArray;

@property (weak, nonatomic) IBOutlet UITextField *workTextField;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;

@property (weak, nonatomic) IBOutlet UITableView *addressTableView;
@property (weak, nonatomic) IBOutlet UIView *workView;
@property (weak, nonatomic) IBOutlet UIView *textFieldView;
@property (weak, nonatomic) IBOutlet UIView *tableViewField;
@property (weak, nonatomic) IBOutlet UIView *buttonsView;
@property (weak, nonatomic) IBOutlet UIButton *removeButton;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@property (strong, nonatomic) Place *currentPlace;

@property (strong, nonatomic) NSArray *addressArray;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sendConstraint;

@property (nonatomic) NSTimer *timer;

@property (strong, nonatomic) InternetConnection *internetConnection;

@property (weak, nonatomic) ErrorsView *errorView;

@end


@implementation ChangeWorkAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.internetConnection = [[InternetConnection alloc] init];
    
    [self getLocation];

    [self addGradientToButton];
    
    self.placesArray = [NSMutableArray new];
    [self.tableViewField setHidden:YES];
    
    if (self.isNewAddress) {
        [self.buttonsView setHidden:YES];
    } else {
        self.addressTextField.text = self.place.address;
        self.workTextField.text = self.place.name;
        [self.sendButton setHidden:YES];
    }
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    
    [self.addressTableView setHidden:YES];
    self.addressArray = [[NSArray alloc] initWithObjects:@"address1",@"address2",@"address3", nil];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
    
    [self.menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
//    UITapGestureRecognizer *dissmissTap = [self.revealViewController tapGestureRecognizer];
//    tap.delegate = self;
//    
//    [self.view addGestureRecognizer:dissmissTap];
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    if ([self.internetConnection isOn] == NO) {
        self.errorView = [self createErrorView];
        [self.errorView internetConnectionError];
        [self.view addSubview:self.errorView];
    }
    
    if (self.place.status != 0){
        self.errorView = [self createErrorView];
        [self.errorView changeWork];
        [self.view addSubview:self.errorView];
    }
    
//    self.revealViewController.delegate = self;

}

-(void)viewWillAppear:(BOOL)animated {
    self.revealViewController.delegate = self;
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
    tap = [self.revealViewController tapGestureRecognizer];
    tap.delegate = self;
    if (position == FrontViewPositionRight) {
        
        [self.view addGestureRecognizer:tap];
    } else if (position == FrontViewPositionLeft) {
        [self.view removeGestureRecognizer:tap];
    }
}

- (void)checkServerError {
    if ([self.internetConnection isOn]) {
        self.errorView = [self createErrorView];
        [self.errorView serverError];
        [self.view addSubview:self.errorView];
    } else {
        self.errorView = [self createErrorView];
        [self.errorView internetConnectionError];
        [self.view addSubview:self.errorView];
    }
}

- (ErrorsView *)createErrorView {
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"ErrorInternetView" owner:self options:nil];
    ErrorsView *errorView = [[ErrorsView alloc] init];
    errorView = [subviewArray objectAtIndex:0];
    errorView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 67, self.view.frame.size.width, self.view.frame.size.height);
    //    self.errorView = [self createErrorView];
    //    [self.errorView serverError];
    //    [self.view addSubview:self.errorView];
    return errorView;
}



- (void)getLocation
{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    switch ([CLLocationManager authorizationStatus]) {
        case kCLAuthorizationStatusAuthorizedAlways:
            break;
        case kCLAuthorizationStatusNotDetermined:
            [self.locationManager requestWhenInUseAuthorization];
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            break;
        case kCLAuthorizationStatusRestricted:
            [self showLocationAlert];
            break;
        case kCLAuthorizationStatusDenied:
            [self showLocationAlert];
            break;
            
        default:
            break;
    }
    
    [self.locationManager startUpdatingLocation];
}


- (void)showLocationAlert
{
    UIAlertView *netAlert = [[UIAlertView alloc] initWithTitle:@"Доступ к геолокации запрещён"
                                                       message:@"Для полноценной работы, приложению требуется доступ к данным геолокации телефона. Перейдите в настройки и включите оределение местоположения."
                                                      delegate:self
                                             cancelButtonTitle:@"Отмена"
                                             otherButtonTitles:@"Настройки", nil];
    [netAlert show];
}

- (void)callAutocomplite {
    GMSPlacesClient *placesClient = [GMSPlacesClient sharedClient];
    
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:CLLocationCoordinate2DMake(self.locationManager.location.coordinate.latitude - 1, self.locationManager.location.coordinate.longitude - 1)
                                                                       coordinate:CLLocationCoordinate2DMake(self.locationManager.location.coordinate.latitude + 1, self.locationManager.location.coordinate.longitude + 1)];
    GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
    filter.type = kGMSPlacesAutocompleteTypeFilterAddress;
    
    [placesClient autocompleteQuery:self.addressTextField.text bounds:bounds filter:filter callback:^(NSArray<GMSAutocompletePrediction *> * _Nullable results, NSError * _Nullable error) {
        {
            NSLog(@"SearchLocation: %@", results);
            [self.placesArray removeAllObjects];
            
            for (GMSAutocompletePrediction* result in results) {
                GMSPlacesClient *placesClient = [GMSPlacesClient sharedClient];
                [placesClient lookUpPlaceID:result.placeID
                                   callback:^(GMSPlace *place, NSError *error) {
                                       Place *singlePlace = [[Place alloc] init];
                                       singlePlace.name = place.name;
                                       singlePlace.address = place.formattedAddress;
                                       singlePlace.longitude = place.coordinate.longitude;
                                       singlePlace.latitude = place.coordinate.latitude;
                                       [self.placesArray addObject:singlePlace];
                                       
                                       [self.addressTableView reloadData];
                                   }];
            }
        }
        
    }];

}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    
}

- (void)dismissKeyboard {
    [self.addressTextField resignFirstResponder];
    [self.workTextField resignFirstResponder];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.addressTableView]) {
        return NO;
    }
    
    return YES;
}
- (void)addGradientToButton {
    
    self.removeButton.layer.cornerRadius = 8;
    self.removeButton.clipsToBounds = YES;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = self.removeButton.bounds;
    
    UIColor *lightGreen = [UIColor colorWithRed:255.0f/255.0f green:57.0f/255.0f blue:7.0f/255.0f alpha:1];
    UIColor *green = [UIColor colorWithRed:205.0f/255.0f green:2.0f/255.0f blue:2.0f/255.0f alpha:1];
    
    gradient.colors = @[(id)lightGreen.CGColor, (id)green.CGColor];
    
    [self.removeButton.layer insertSublayer:gradient atIndex:0];
}

-(IBAction)send:(id)sender {
    User *user = [User currentUser];
    
    if (user.place.count == 2) {
        [self showAllertViewWithTittle:@"Помилка" andMessage:@"Додано максимально можливу кількість робочих місць!"];
        return;
    }
    
    if (!(self.workTextField.text.length == 0) && !(self.addressTextField.text.length == 0)) {
        [[RequestManager sharedManager] addNewWork:self.currentPlace.name withAddress:self.currentPlace.address longtitude:self.currentPlace.longitude latitude:self.currentPlace.latitude succesBlock:^(NSInteger success) {
            
            [self showAllertViewWithTittleAction:@"Успіх" andMessage:@"Нове робоче місце успішно додано!"];
//            [self.navigationController popViewControllerAnimated:YES];
        } onFalureBlock:^(NSInteger statusCode) {
            [self checkServerError];
        }];
    } else {
       [self showAllertViewWithTittle:@"Помилка" andMessage:@"Заповніть усі поля"];
    }
}

-(IBAction)removeWork:(id)sender {
    [[RequestManager sharedManager] changeWork:self.place.name withAddress:self.place.address placeId:@(self.place.placeId) longtitude:self.place.longitude latitude:self.place.latitude status:2 succesBlock:^(NSInteger success) {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        AcceptViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"AcceptViewController"];
        vc.tittleString = @"Вашу заявку на видалення місця роботи надіслано!";
        [self.navigationController pushViewController:vc animated:YES];

//        
//        [self showAllertViewWithTittle:@"Успех" andMessage:@"Рабочее место успешно удаленно!"];
        
    } onFalureBlock:^(NSInteger statusCode) {
        [self checkServerError];
    }];
}

-(IBAction)change:(id)sender {
    if (self.currentPlace.name == nil) {
        self.currentPlace.name = self.place.name;
    }
    if (self.currentPlace.address == nil) {
        self.currentPlace.address = self.place.address;
    }
    if (self.currentPlace.longitude == 0) {
        self.currentPlace.longitude = self.place.longitude;
    }
    if (self.currentPlace.latitude == 0) {
        self.currentPlace.latitude = self.place.latitude;
    }
    
    if (self.currentPlace.name == nil) {
        self.currentPlace = self.place;
    }
    
    [[RequestManager sharedManager] changeWork:self.currentPlace.name withAddress:self.currentPlace.address placeId:@(self.place.placeId) longtitude:self.currentPlace.longitude latitude:self.currentPlace.latitude status:1 succesBlock:^(NSInteger success) {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        AcceptViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"AcceptViewController"];
        vc.tittleString = @"Вашу заявку на зміну місця роботи надіслано!";
        [self.navigationController pushViewController:vc animated:YES];
        
//        [self showAllertViewWithTittle:@"Успех" andMessage:@"Рабочее место успешно изменено!"];
        
    } onFalureBlock:^(NSInteger statusCode) {
        [self checkServerError];
    }];
    
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.addressTextField) {
        [self.tableViewField setHidden:NO];
        [self.addressTableView setHidden:NO];
        [self.workView setHidden:YES];
        [self addBorderToView:self.textFieldView];
        [self addBorderToView:self.tableViewField];
        if (self.isNewAddress) {
            [self.buttonsView setHidden:YES];
        }
    }

}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.addressTextField) {
        [self.timer invalidate];
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(callAutocomplite) userInfo:nil repeats:NO];
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if (textField == self.addressTextField) {
        [self.tableViewField setHidden:YES];
        [self removeBorderToView:self.textFieldView];
        [self removeBorderToView:self.tableViewField];
        [self.workView setHidden:NO];
        [self.addressTableView setHidden:YES];
        if (self.isNewAddress) {
            [self.buttonsView setHidden:NO];
        }
    }
    return YES;
}

- (void) addBorderToView:(UIView*)view {
    view.layer.cornerRadius = 8;
    view.clipsToBounds = YES;
    view.layer.borderWidth = 2;
    view.layer.borderColor = [UIColor colorWithRed:0.0f/255.0f green:191.0f/255.0f blue:158.0f/255.0f alpha:1].CGColor;
}

- (void) removeBorderToView:(UIView*)view {
    view.layer.cornerRadius = 8;
    view.clipsToBounds = YES;
    view.layer.borderWidth = 2;
    view.layer.borderColor = [UIColor colorWithRed:251.0f/255.0f green:250.0f/255.0f blue:255.0f/255.0f alpha:1].CGColor;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Place *place = self.placesArray[indexPath.row];
    self.addressTextField.text = place.address;
    self.currentPlace = place;
    [self removeBorderToView:self.textFieldView];
    [self removeBorderToView:self.tableViewField];
    [self.addressTextField resignFirstResponder];
    [self.addressTableView setHidden:YES];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.placesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AddressCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddressCell"];
    Place *place = self.placesArray[indexPath.row];
    cell.address.text = place.address;
    return cell;
}


#pragma mark - AllertView
- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    UIAlertController *alertController = [UIAlertController new];
    
    alertController = [UIAlertController alertControllerWithTitle:tittle message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }]];
    [self presentViewController:alertController animated:YES completion:NULL];
}
- (void)showAllertViewWithTittleAction:(NSString*) tittle andMessage:(NSString*) message {
    UIAlertController *alertController = [UIAlertController new];
    
    alertController = [UIAlertController alertControllerWithTitle:tittle message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    [self presentViewController:alertController animated:YES completion:NULL];
}

@end
