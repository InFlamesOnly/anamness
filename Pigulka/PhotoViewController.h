//
//  PhotoViewController.h
//  Pigulka
//
//  Created by macOS on 18.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PhotoViewController : UIViewController

@property (strong, nonatomic) NSString *requestString;

@end

NS_ASSUME_NONNULL_END
