//
//  Research.h
//  Pigulka
//
//  Created by macOS on 14.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Research : NSObject

@property (strong, nonatomic) NSNumber *geoEnable;
@property (strong, nonatomic) NSString *researchId;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *activeTill;
@property (strong, nonatomic) NSString *toEnd;
@property (strong, nonatomic) NSString *policyURL;
@property (strong, nonatomic) NSString *formURL;
@property BOOL isOtherResearch;
@property BOOL isPrivacy;

- (id)initWithServerResponse:(NSDictionary*)response;

@end
