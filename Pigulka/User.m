//
//  User.m
//  Pigulka
//
//  Created by macOS on 14.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "User.h"

@implementation User

-(id)initWithServerResponse:(NSDictionary*)response {
    self = [super init];
    if (self) {
        self.phone = [[response valueForKey:@"user"] valueForKey:@"phone"];
        self.middleName = [[response valueForKey:@"user"] valueForKey:@"middle_name"];
        self.token = [[response valueForKey:@"user"] valueForKey:@"token"];
        self.name = [[response valueForKey:@"user"] valueForKey:@"name"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.phone forKey:@"phone"];
    [encoder encodeObject:self.middleName forKey:@"middleName"];
    [encoder encodeObject:self.token forKey:@"tokken"];
    [encoder encodeObject:self.name forKey:@"name"];
}

+ (void)saveUser:(User *)user {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm addObject:user];
    [realm commitWriteTransaction];
}

+ (User *)currentUser {
    return [[self allObjects] objectAtIndex:0];
}

+ (void)updateUser:(User*)newUser {
    User *user = [User currentUser];
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    user.bonuses = newUser.bonuses;
    user.place = newUser.place;
    [realm commitWriteTransaction];
}

@end
