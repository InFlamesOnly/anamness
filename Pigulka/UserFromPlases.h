//
//  UserFromPlases.h
//  Pigulka
//
//  Created by macOS on 25.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserFromPlases : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *surname;
@property (strong, nonatomic) NSString *middleName;
@property (strong, nonatomic) NSString *region;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *profession;
@property (strong, nonatomic) NSString *experience;
@property (strong, nonatomic) NSString *position;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSString *bonuses;
@property (strong, nonatomic) NSArray *plases;

-(id)initWithServerResponse:(NSDictionary*)response;

@end
