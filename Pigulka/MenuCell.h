//
//  MenuCell.h
//  Pigulka
//
//  Created by macOS on 12.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cellName;

@end
