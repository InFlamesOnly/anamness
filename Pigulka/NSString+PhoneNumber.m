//
//  NSString+PhoneNumber.m
//  Pigulka
//
//  Created by macOS on 07.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "NSString+PhoneNumber.h"

@implementation NSString (PhoneNumber)

+ (NSString *) removeAllCharacteFromValidatePhoneNumber:(NSString *)phoneNumber {
    NSString *string = phoneNumber;
    NSCharacterSet *trim = [NSCharacterSet characterSetWithCharactersInString:@" ()-"];
    NSString *result = [[string componentsSeparatedByCharactersInSet:trim] componentsJoinedByString:@""];
    NSLog(@"%@", result);
    return result;
}

@end
