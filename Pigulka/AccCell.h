//
//  AccCell.h
//  Pigulka
//
//  Created by macOS on 15.11.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AccCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *value;
@property (weak, nonatomic) IBOutlet UIImageView *acceptImage;

@end

NS_ASSUME_NONNULL_END
