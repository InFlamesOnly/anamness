//
//  PhotoViewController.m
//  Pigulka
//
//  Created by macOS on 18.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

#import "PhotoViewController.h"
#import "SWRevealViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "NPCameraView.h"
#import "FormsViewController.h"


@interface PhotoViewController () <SWRevealViewControllerDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *menuButton;

@property (weak, nonatomic) IBOutlet UIView *cameraView;
@property (weak, nonatomic) IBOutlet UIImageView *stillImageView;
@property (weak, nonatomic) IBOutlet UIButton *takePhotoButton;

@property (weak, nonatomic) IBOutlet UIImageView *transformedPhoto;

@property (weak, nonatomic) IBOutlet UIView *photoView;

@property (strong, nonatomic) IBOutlet NPCameraView *camera;

@property (weak, nonatomic) IBOutlet UIView *acceptPhotoView;
@property (weak, nonatomic) IBOutlet UIImageView *acceptImageView;

@property (strong, nonatomic) UIImage *savedImage;


@end


@implementation PhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    if (self.revealViewController.panGestureRecognizer != nil) {
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    self.takePhotoButton.layer.cornerRadius = self.takePhotoButton.frame.size.height / 2;
    self.takePhotoButton.layer.masksToBounds = YES;
    [self.acceptPhotoView setHidden:YES];
    
    dispatch_async(dispatch_get_main_queue(), ^(void){
            self.camera = [[NPCameraView alloc] initWithFrame:self.view.frame];
        dispatch_async(dispatch_get_main_queue(), ^(void){
              [self.cameraView addSubview:self.camera];
        });
    });

}

-(void)viewWillAppear:(BOOL)animated {
    self.revealViewController.delegate = self;
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
    tap = [self.revealViewController tapGestureRecognizer];
    tap.delegate = self;
    if (position == FrontViewPositionRight) {
        [self.view addGestureRecognizer:tap];
    } else if (position == FrontViewPositionLeft) {
        [self.view removeGestureRecognizer:tap];
    }
}


- (IBAction)captureImageButtonPressed:(id)sender {
    [self.camera captureImageWithCallback:^(UIImage *image) {
        self.stillImageView.image = image;
        UIImage *transformedImage = image;
        transformedImage = [self cropImage:image fromNeedFrame:self.photoView.frame];
        self.transformedPhoto.image = [self cropImage:image fromNeedFrame:self.photoView.frame];
        self.savedImage = transformedImage;
        [self showAcceptViewWithImage:transformedImage];
        
    }];
}

- (IBAction)acceptPhoto:(id)sender {
    UIImageWriteToSavedPhotosAlbum(self.savedImage, nil, nil, nil);
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FormsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"FormsViewController"];
    vc.requestString = self.requestString;
    [self.navigationController pushViewController:vc animated:YES];
    [self.acceptPhotoView setHidden:YES];
    NSLog(@"ACCEPT");
}

- (IBAction)cancel:(id)sender {
    [self.acceptPhotoView setHidden:YES];
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showAcceptViewWithImage:(UIImage*)image {
    self.acceptImageView.image = image;
    [self.acceptPhotoView setHidden:NO];
}

- (UIImage*)cropImage:(UIImage*)image fromNeedFrame:(CGRect)frame {
    CGRect newFrame = CGRectMake(frame.origin.x + 48 , frame.origin.y, frame.size.width, frame.size.height - 16);
    UIImage *cropedImage = [self normalizedImage:image];
    CGImageRef drawImage = CGImageCreateWithImageInRect(cropedImage.CGImage, newFrame);
    UIImage *newImage = [UIImage imageWithCGImage:drawImage];
    CGImageRelease(drawImage);
    return newImage;
}

- (UIImage *)normalizedImage:(UIImage*)image {
    if (image == UIImageOrientationUp) return image;
    
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    [image drawInRect:(CGRect){0, 0, image.size}];
    UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return normalizedImage;
}

@end
