//
//  HistoryBonusessCell.h
//  Pigulka
//
//  Created by Dima on 19.03.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HistoryBonusessCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *htmlText;
@property (weak, nonatomic) IBOutlet UIView *backgroundShadowView;

@end

NS_ASSUME_NONNULL_END
